var path = require("path");
var markdownpdf = require("markdown-pdf");


function exportOnePdf(mdFilePath = '/Users/vincent/code/sp-fe-docs-gitlab/docs/README.md') {
  try {
    const relativePath = path.relative(path.resolve(process.cwd(), "docs"), mdFilePath)

    const destFilePath = `${path.dirname(path.resolve(process.cwd(), "public", relativePath))}/${path.basename(mdFilePath, '.md')}.pdf`

    // console.log('---', '\n', path.resolve(process.cwd(),'github-markdown.css'))
    var options = {
      cwd: path.dirname(mdFilePath),
      cssPath: path.resolve(process.cwd(),'markdown.css'),
      highlightCssPath: path.resolve(process.cwd(),'hljs.css')
      // remarkable: {
      //   html: true,
      //   breaks: true,
      //   plugins: [require('remarkable-classy')],
      //   syntax: ['footnote', 'sup', 'sub']
      // }
    }
    markdownpdf(options).from(mdFilePath).to(destFilePath, function () {
      console.log(`export ${destFilePath} done`)
    });
  } catch (error) {
    console.log('error', '\n', error);
  }

}

module.exports = exportOnePdf






