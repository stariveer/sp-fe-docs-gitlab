

# 表设计

+ info_r_user_resource
+ info_r_user_resource_group
+ info_video_resource
+ info_video_resource_group
+ info_video_source

# 接口设计

## cyclist-controller
+ 获取指定条件的骑手抓拍计数
+ 根据指定条件过滤骑手抓拍

## vehicle-controller
+ 获取指定条件的车辆抓拍计数
+ 根据指定条件过滤车辆抓拍