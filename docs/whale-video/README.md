

# 表设计

+ info_r_user_resource
+ info_r_user_resource_group
+ info_video_resource
+ info_video_resource_async
+ info_video_resource_async_log
+ info_video_resource_group
+ info_video_resource_sip
+ info_video_source
+ info_video_source_type
+ sync_camera_viper

# 接口设计

## vehicle-struct-controller
+ 批量接入结构化解析任务
+ 批量取消接入车辆结构化解析任务
+ 获取结构化查询摄像头列表

## video-controller
+ 新增视频源
+ 删除视频源
+ 视频源统计数量预览
+ 视频源移入新的分组
+ 获取视频源列表
+ 恢复删除的视频源
+ 视频源标记热区
+ 视频回放
+ rtsp摄像机获取视频流地址
+ 获取视频源分类信息
+ 视频源详情
+ 编辑视频源
+ 自动标记视频源

## video-group-controller
+ 新增视频源分组
+ 获取视频源树结构
+ 查看视频源分组详情
+ 编辑视频源分组
+ 删除视频源分组






