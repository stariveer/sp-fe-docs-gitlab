

# 数据表结构SQL
```sql

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

```

## `emergency_task`

```sql

-- ----------------------------
--  Table structure for `emergency_task`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_task`;
CREATE TABLE `emergency_task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_serial` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '任务编号',
  `name` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `long_run` tinyint(1) DEFAULT 0 COMMENT '任务是否长期有效，1为长期有效，为0则关注开始和结束时间,默认为0',
  `start_day` timestamp NULL DEFAULT NULL COMMENT '任务开始的日期',
  `end_day` timestamp NULL DEFAULT NULL COMMENT '任务结束的日期',
  `the_whole_day` tinyint(1) DEFAULT 0 COMMENT '该任务是否全天运行，1为全天运行，否则指定时间段运行,默认为0',
  `working_times` text COLLATE utf8mb4_bin DEFAULT NULL COMMENT '任务调度时间json串',
  `working` tinyint(1) DEFAULT 0 COMMENT '该任务是否处于运行状态',
  `remark` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '任务备注信息',
  `status` tinyint(4) DEFAULT 1 COMMENT '任务是否有效，即任务是否处于运行时间之内',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `task_scenario` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '任务场景',
  `parking_duration_threshold` int(4) DEFAULT NULL COMMENT '停车阈值',
  PRIMARY KEY (`id`),
  KEY `idx_serial` (`task_serial`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

```

## `emergency_task_ref_camera`

```sql

-- ----------------------------
--  Table structure for `emergency_task_ref_camera`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_task_ref_camera`;
CREATE TABLE `emergency_task_ref_camera` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `camera_serial` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '视频源编号',
  `task_id` bigint(20) NOT NULL COMMENT '任务编号',
  `status` tinyint(4) DEFAULT 1 COMMENT '下发任务任务的状态',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=389 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='智能应急任务和摄像头关联表';

```

## `emergency_task_ref_type`

```sql

-- ----------------------------
--  Table structure for `emergency_task_ref_type`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_task_ref_type`;
CREATE TABLE `emergency_task_ref_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` bigint(20) NOT NULL COMMENT '任务表主键',
  `type` bigint(20) NOT NULL COMMENT '任务类型',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='任务类型关联表';

```

## `emergency_task_schedule_log`

```sql

-- ----------------------------
--  Table structure for `emergency_task_schedule_log`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_task_schedule_log`;
CREATE TABLE `emergency_task_schedule_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` bigint(20) NOT NULL COMMENT '任务外键',
  `next_trigger_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '触发时间',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='任务调度日志表';

```

## `emergency_task_type`

```sql

-- ----------------------------
--  Table structure for `emergency_task_type`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_task_type`;
CREATE TABLE `emergency_task_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 NOT NULL COMMENT '类型',
  `type` int(10) unsigned NOT NULL COMMENT '类型编号',
  `object_type` varchar(64) CHARACTER SET utf8mb4 NOT NULL COMMENT 'vps任务类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='任务类型表';

```

## `info_platform`

```sql

-- ----------------------------
--  Table structure for `info_platform`
-- ----------------------------
DROP TABLE IF EXISTS `info_platform`;
CREATE TABLE `info_platform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `platform_serial` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '序号',
  `platform_name` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '平台名称',
  `protocol` int(11) DEFAULT NULL COMMENT '传输协议：1、UDP；2、TCP；3、TCP server',
  `source_id` bigint(20) DEFAULT NULL COMMENT '平台厂商',
  `platform_id` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '平台ID',
  `our_platform_id` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '我方平台ID',
  `platform_ip` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '平台IP',
  `port` int(11) NOT NULL COMMENT '端口',
  `username` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
  `password` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `platform_type` tinyint(4) NOT NULL COMMENT '平台类型：1、GB28181；2、GAT1400',
  `task_id` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'GAT1400平台任务ID',
  `expand` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '拓展字段',
  `remark` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT NULL COMMENT '状态：1、有效；2、已删除；3、添加失败',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='平台管理表';

```

## `info_r_user_resource`

```sql

-- ----------------------------
--  Table structure for `info_r_user_resource`
-- ----------------------------
DROP TABLE IF EXISTS `info_r_user_resource`;
CREATE TABLE `info_r_user_resource` (
  `user_resource_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门视频源Id',
  `resource_id` bigint(20) NOT NULL COMMENT '视频源id',
  `user_id` bigint(20) NOT NULL COMMENT '部门id',
  `permission_type` int(11) NOT NULL COMMENT '权限类型 0：查看权限 1：事务权限',
  `group_id` bigint(20) DEFAULT NULL COMMENT '分配权限时的视频源分组',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '权限状态0失效1生效',
  PRIMARY KEY (`user_resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

```

## `info_r_user_resource_group`

```sql

-- ----------------------------
--  Table structure for `info_r_user_resource_group`
-- ----------------------------
DROP TABLE IF EXISTS `info_r_user_resource_group`;
CREATE TABLE `info_r_user_resource_group` (
  `user_resource_group_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门视频源分组id',
  `resource_group_id` bigint(20) NOT NULL COMMENT '视频源分组id',
  `user_id` bigint(20) NOT NULL COMMENT '部门id',
  `permission_type` int(11) NOT NULL COMMENT '0:查看权限 1：事务权限',
  `current_level` int(11) NOT NULL DEFAULT 0 COMMENT '分配本分区视频源1',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '权限状态0失效1生效',
  PRIMARY KEY (`user_resource_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

```

## `info_subscribe`

```sql

-- ----------------------------
--  Table structure for `info_subscribe`
-- ----------------------------
DROP TABLE IF EXISTS `info_subscribe`;
CREATE TABLE `info_subscribe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `platform_id` bigint(20) NOT NULL COMMENT '平台主键',
  `receive_addr` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '接收地址',
  `subscribe_id` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '订阅ID',
  `resource_uri` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订阅资源路径',
  `title` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订阅标题',
  `subscribe_detail` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订阅类别',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态：1、订阅成功；2、取消订阅',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `serial` (`platform_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='gat1400订阅记录表';

```

## `info_system_settings` 暂时无用

```sql

-- ----------------------------
--  Table structure for `info_system_settings` 暂时无用
-- ----------------------------
DROP TABLE IF EXISTS `info_system_settings`;
CREATE TABLE `info_system_settings` (
  `sys_set_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '系统配置ID',
  `name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统配置中文名称，language：1-zh ，2-en',
  `code` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统配置编码',
  `type` int(11) DEFAULT NULL COMMENT '类型 1：系统配置 2：厂家配置',
  `value` longtext COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置值',
  `remark` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `category` int(11) DEFAULT 0 COMMENT '模块分类 1:地图管理 2:基础设置 3:功能模块设置 4:其他设置 5:系统信息 6:系统维护 7:GB28181配置参数',
  PRIMARY KEY (`sys_set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统配置表';

```

## `info_video_resource`

```sql

-- ----------------------------
--  Table structure for `info_video_resource`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource`;
CREATE TABLE `info_video_resource` (
  `resource_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '视频源ID',
  `serial` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '视频源serial',
  `resource_type_id` bigint(20) DEFAULT NULL COMMENT '视频源类型ID',
  `type` int(11) DEFAULT 0 COMMENT '摄像头类型',
  `group_id` int(11) DEFAULT NULL COMMENT '分组id',
  `protocol_type` smallint(6) DEFAULT NULL COMMENT '网络协议类型',
  `name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `device_code` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '设备编码',
  `longitude` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '经度',
  `latitude` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '纬度',
  `path` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '视频地址',
  `status` int(11) DEFAULT 1 COMMENT '状态',
  `access_status` int(11) DEFAULT 0 COMMENT '接入状态（0--未接入，1-已接入）',
  `run_status` int(11) DEFAULT 0 COMMENT '运行状态（0--离线，1--在线）',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `organ_id` bigint(20) DEFAULT NULL COMMENT '组织结构ID',
  `source_id` bigint(20) DEFAULT NULL COMMENT '来源ID',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `username` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '抓拍机用户名',
  `password` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '抓拍机密码',
  `platform_serial` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '平台服务serial',
  `ratio_type` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '默认SD标清,HD高清',
  `Dir` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'viper抓拍机Dir字段',
  `body_access_status` int(1) DEFAULT 0 COMMENT '接入状态（0--未接入，1-已接入，2-接入中，3-接入失败）',
  `city_access_status` int(1) NOT NULL DEFAULT 0 COMMENT '智慧城市接入状态（0：未接入   1：已接入）',
  `access_scenario` int(1) DEFAULT 0 COMMENT '智慧城市接入场景',
  `replay_platform_id` bigint(20) DEFAULT NULL COMMENT '回放平台id',
  `roi` text COLLATE utf8mb4_bin DEFAULT NULL COMMENT '标记热区json串',
  `structural_search` tinyint(1) DEFAULT 0 COMMENT '是否用作结构化查询',
  `structural_enabled` tinyint(1) DEFAULT 0 COMMENT '是否接入结构化查询',
  `labelling_type` int(11) DEFAULT -1 COMMENT '标注类型 -1-未知（新建） 0-手动标注 1-自动标注',
  `labelling_status` int(11) DEFAULT 0 COMMENT '标注状态 0-未标注 1-标注中 2-标注成功 3-标注失败',
  `labelling_img_minio_key` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '自动标注生成图片在minio的key',
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `uniq_serial` (`serial`),
  KEY `idx_update_time` (`update_time`)
) ENGINE=InnoDB AUTO_INCREMENT=100301 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频相关资源，根据类型处理相关数据。path：为本地';

```

## `info_video_resource_async`

```sql

-- ----------------------------
--  Table structure for `info_video_resource_async`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource_async`;
CREATE TABLE `info_video_resource_async` (
  `async_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '异步下发ID',
  `resource_id` bigint(20) DEFAULT NULL COMMENT '视频源ID',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '名字',
  `async_serial` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '异步编码',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `remark` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT 1 COMMENT '状态（0--删除，1--正常）',
  `object_type` int(1) DEFAULT 1 COMMENT '1:人脸 2：人体',
  PRIMARY KEY (`async_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频源同步';

```

## `info_video_resource_async_log`

```sql

-- ----------------------------
--  Table structure for `info_video_resource_async_log`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource_async_log`;
CREATE TABLE `info_video_resource_async_log` (
  `async_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `async_id` bigint(20) DEFAULT NULL COMMENT '异步下发ID',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '日志时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `discription` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `remark` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`async_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频源异步服务日志可用作状态查询status  0 为失败，1为成功。';

```

## `info_video_resource_group`

```sql

-- ----------------------------
--  Table structure for `info_video_resource_group`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource_group`;
CREATE TABLE `info_video_resource_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分组ID',
  `serial` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '视频源组serial',
  `organ_id` int(11) DEFAULT NULL COMMENT '组织机构id',
  `name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `region` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区域坐标json串',
  `code` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '编码',
  `path` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '递归字段',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级分组id',
  `remark` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注字段',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() COMMENT '更新时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `creator_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人名称',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  `status` int(11) DEFAULT 1 COMMENT '删除标记0--删除',
  `zoom_level` int(11) DEFAULT NULL COMMENT '地图层级',
  `center_longitude` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '中心点经度',
  `center_latitude` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '中心点纬度',
  `updater_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人名称',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `uniq_serial` (`serial`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='递归字段用来查询分组所属层级关系如 0-1010101-01010101';

```

## `info_video_resource_sip`

```sql

-- ----------------------------
--  Table structure for `info_video_resource_sip`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource_sip`;
CREATE TABLE `info_video_resource_sip` (
  `sip_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'sip服务器ID',
  `platform_id` bigint(20) NOT NULL COMMENT '接入平台id',
  `resource_id` bigint(20) NOT NULL COMMENT '视频源ID',
  `sip_play_back` int(11) DEFAULT NULL COMMENT '是否录像回访0-否1-是',
  `sip_call_begin_time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回放开始时间',
  `sip_call_end_time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回放结束时间',
  `speed` float DEFAULT NULL COMMENT '回放速率',
  `replay_id` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回放ID',
  `raw_config` longtext COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'gb,1400相机的附加配置信息',
  `status` int(11) DEFAULT 1 COMMENT '状态位',
  PRIMARY KEY (`sip_id`),
  KEY `idx_resource_id` (`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='sip信息表';

```

## `info_video_resource_type` 暂时无用

```sql

-- ----------------------------
--  Table structure for `info_video_resource_type` 暂时无用
-- ----------------------------
DROP TABLE IF EXISTS `info_video_resource_type`;
CREATE TABLE `info_video_resource_type` (
  `resource_type_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '视频源类型ID',
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型名称',
  `code` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '编码',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `remark` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`resource_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频源类型，0为本地视频，1为摄像头RTSP，2为抓拍机，3以上为各级平台。数据字典，固定。';

```

## `info_video_source`

```sql

-- ----------------------------
--  Table structure for `info_video_source`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_source`;
CREATE TABLE `info_video_source` (
  `source_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '来源ID',
  `name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `code` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '编码',
  `source_type` int(11) DEFAULT NULL COMMENT '厂商: 2、RTSP；3、抓拍机；4、NVR；5、ONVIF；6、GB28181；7、GAT1400',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `address` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址',
  `description` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `remark` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频来源';

```

## `info_video_source_type`

```sql

-- ----------------------------
--  Table structure for `info_video_source_type`
-- ----------------------------
DROP TABLE IF EXISTS `info_video_source_type`;
CREATE TABLE `info_video_source_type` (
  `type_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '一级类型',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '一级类型名称',
  `status` int(11) DEFAULT NULL COMMENT '状态：0、已删除；1、有效',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频来源类型';

```

## `sync_camera_server`

```sql

-- ----------------------------
--  Table structure for `sync_camera_server`
-- ----------------------------
DROP TABLE IF EXISTS `sync_camera_server`;
CREATE TABLE `sync_camera_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `camera` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '视频源唯一标识',
  `server` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '视频源接入的服务器',
  `service_type` tinyint(3) NOT NULL COMMENT '服务类型，1：人脸；2：结构化',
  `current_access` tinyint(3) NOT NULL COMMENT '是否当前接入，1：当前接入；0：历史接入',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `sync_csr` (`camera`,`server`,`service_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频源服务器关联表';

```

## `sync_camera_viper`

```sql

-- ----------------------------
--  Table structure for `sync_camera_viper`
-- ----------------------------
DROP TABLE IF EXISTS `sync_camera_viper`;
CREATE TABLE `sync_camera_viper` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `camera` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '视频源唯一标识',
  `server` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `region_id` int(7) DEFAULT NULL COMMENT '对应viper的regionId',
  `camera_id` tinyint(3) DEFAULT NULL COMMENT '对应viper的cameraId',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `sync_cv` (`camera`,`server`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='视频源viper标识关联表';

```

## `sync_event_record`

```sql

-- ----------------------------
--  Table structure for `sync_event_record`
-- ----------------------------
DROP TABLE IF EXISTS `sync_event_record`;
CREATE TABLE `sync_event_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `event_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '时间名称',
  `url` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '请求url',
  `http_method` varchar(10) COLLATE utf8mb4_bin NOT NULL COMMENT 'http方法',
  `content_type` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '请求内容类型',
  `request` longblob DEFAULT NULL COMMENT '请求参数',
  `master_response` longblob DEFAULT NULL COMMENT '主节点返回',
  `send_flag` tinyint(3) DEFAULT 1 COMMENT '是否已发送，1：已发送；0：未发送',
  `key` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '标识事件对应的主节点信息，CREATE_TARGET_LIBRARY事件存储的是主节点${source}_${dbID}，CREATE_TARGET事件存储的是主节点${masterDbId}_${featureId}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='sync事件记录表';

```

## `sync_event_record_failure`

```sql

-- ----------------------------
--  Table structure for `sync_event_record_failure`
-- ----------------------------
DROP TABLE IF EXISTS `sync_event_record_failure`;
CREATE TABLE `sync_event_record_failure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `url` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '请求url',
  `http_method` varchar(10) COLLATE utf8mb4_bin NOT NULL COMMENT '请求方法',
  `content_type` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '请求参数类型',
  `request` longblob DEFAULT NULL COMMENT '请求参数',
  `uuid` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'uuid用于唯一标识一条记录',
  `fail` tinyint(3) DEFAULT 0 COMMENT '是否失败标识，0：成功，1：失败',
  `times` tinyint(3) DEFAULT 0 COMMENT '重试次数',
  `event_id` bigint(20) DEFAULT NULL COMMENT '对应于sync_event_record的id',
  PRIMARY KEY (`id`),
  KEY `idx_times` (`times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='请求异常且异常处理失败（包括回滚和重试）事件记录表';

```

## `sync_server_config`

```sql

-- ----------------------------
--  Table structure for `sync_server_config`
-- ----------------------------
DROP TABLE IF EXISTS `sync_server_config`;
CREATE TABLE `sync_server_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server_type` tinyint(4) NOT NULL COMMENT '服务器引擎类型，1：静态人脸比对引擎；2：动态人脸比对引擎（图片流）；3：动态人脸比对引擎（视频流）；4：动态人脸比对引擎（图片与视频混合）；5：视频结构化引擎（人体车辆）',
  `server` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '服务器列表以逗号分隔，若不带端口号默认为30080',
  `feature_version` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '特征版本号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='服务配置表';

```

## `sync_tar_lib_slave`

```sql

-- ----------------------------
--  Table structure for `sync_tar_lib_slave`
-- ----------------------------
DROP TABLE IF EXISTS `sync_tar_lib_slave`;
CREATE TABLE `sync_tar_lib_slave` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `master_db_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主人像库名称',
  `master_db_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主人像库id',
  `master_db_type` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主节点人像库类型，0：静态库；1：布控库',
  `slave_db_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '从人像库id',
  `slave_address` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '从人像库所属服务地址',
  `event_id` bigint(20) NOT NULL COMMENT '对应于sync_event_record的id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dbr` (`master_db_id`,`slave_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='人像库主从关系表';

```

## `sync_target_slave`

```sql

-- ----------------------------
--  Table structure for `sync_target_slave`
-- ----------------------------
DROP TABLE IF EXISTS `sync_target_slave`;
CREATE TABLE `sync_target_slave` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `master_db_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主人像库id',
  `master_feature_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主服务器特征id',
  `master_image_url` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '主服务器图片url',
  `slave_db_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '从人像库id',
  `slave_feature_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '从服务器特征值id',
  `slave_image_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '从服务器图片url',
  `slave_address` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '从服务器地址',
  `event_id` bigint(20) NOT NULL COMMENT '对应于sync_event_record的id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `imgr` (`master_db_id`,`master_feature_id`,`slave_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='人像主从关系表';

SET FOREIGN_KEY_CHECKS = 1;
```