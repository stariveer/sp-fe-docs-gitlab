

# 表设计

+ emergency_task
+ emergency_task_ref_camera
+ emergency_task_ref_type
+ emergency_task_schedule_log
+ emergency_task_type
+ sync_camera_viper

# 接口设计

## task-controller
+ 创建智能应急任务
+ 获取智能应急任务列表
+ 查看智能应急任务详情
+ 修改任务信息
+ 设备重新上线
+ 重启任务
+ 终止任务
