

# 如何维护polaris-document

## 写新文章

1. 在`/docs/`以及`/docs/任意层级的子文件夹内`新建**文件夹**,推荐以小写+连字符(aa-xy)格式命名
2. 在刚才新建的文件夹内新建`README.md`文件
3. 也可以起名不为"`README.md`",但会影响路由:

注意路由最后是否带`/`会有[[区别]](https://vuepress.vuejs.org/zh/guide/directory-structure.html#%E9%BB%98%E8%AE%A4%E7%9A%84%E9%A1%B5%E9%9D%A2%E8%B7%AF%E7%94%B1)

|   文件的相对路径    |  页面路由地址   |
|--------------------|----------------|
| `/README.md`       | `/`            |
| `/guide/README.md` | `/guide/`      |
| `/config.md`       | `/config.html` |

## 关于图片

1. 优先使用相对路径,如`![pic.jpg](./pic.jpg)`,尽量将文章相关的图片和其`.md`文件放置于同一文件夹内,方便日后调整目录
2. 图片文件名称尽量使用**英语+数字**,如果实在要用中文,请注意不要带空格

## 本地编写

1. 本地写文档可以使用**vscode**[[下载]](https://code.visualstudio.com/),可以直接在编辑器内预览编译后的效果:
![preview](./preview.png)

## 维护路由和导航

1. 编辑文件`docs/.vuepress/config.js`, `themeConfig.nav`部分,见[[api]](https://vuepress.vuejs.org/zh/theme/default-theme-config.html#%E5%AF%BC%E8%88%AA%E6%A0%8F-logo)
2. `md`文件顶部加入以下代码可以自动生成文章的导航侧边栏
```yaml

```


