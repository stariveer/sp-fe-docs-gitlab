# 如何将一篇文档导出为pdf格式1

## 1.在chrome 浏览器安装插件**Markdown Viewer**

![Markdown Viewer chrome 网上应用店](./1607931557602.jpg)

- [[chrome 网上应用店地址]](https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk/related)
- fallback:如果无法打开，也可以下载插件本地安装[MarkdownViewer.crx](./MarkdownViewer.crx)

## 2.配置Markdown Viewer

### 允许Markdown Viewer访问线上`.md`文件
点击插件选项菜单：
![配置](./1607932550162.jpg)

点击右侧“`ALLOW ALL`”，表示允许打开所有域名的markdown文件
![配置1](./1607933181238.jpg)

### 允许Markdown Viewer访问本地`.md`文件
> 如果需要打开本地markdown文件，可以点击下方“`Allow Access to file:// URLs`”按钮，并按照下图打开插件浏览本地文件的权限：

![配置1](./WeChat35c56a68353e8c1fccb69e2291371f65.png)

## 3.在gitlab中打开欲导出为pdf的markdown文件

### 打开markdown raw文件，由Markdown Viewer渲染成网页
如：[https://gitlab.sz.sensetime.com/its-engineering/sensepolaris/polaris-document/blob/master/docs/guide/export-to-pdf/README.md](https://gitlab.sz.sensetime.com/its-engineering/sensepolaris/polaris-document/blob/master/docs/guide/export-to-pdf/README.md)

![WeChat240d5b504e8f7a83d709f6a3bdb5c9fa.png](./WeChat240d5b504e8f7a83d709f6a3bdb5c9fa.png)

点击右侧“`Open raw`”按钮，将在新标签页打开由Markdown Viewer渲染的网页，如下图：
![WeChat621700b24e523cc609ee767e3f6d3b8d.png](./WeChat621700b24e523cc609ee767e3f6d3b8d.png)

### 将此网页导出为pdf文件

在chrome中点击快捷键`ctrl + P`，打开打印预览界面，目标打印机选择“另存为PDF”,
取消“更多设置下”的“页眉和页脚”，最后点击“保存”，即可将整篇文档导出为pdf格式。

![WeChat818bd5423e99c66db1a4b4ef0a27a2fd.png](./WeChat818bd5423e99c66db1a4b4ef0a27a2fd.png)

the end