---
home: true
heroImage: /images/hero.png
heroText: SensePolaris
tagline: 商汤睿途智慧交通平台
actionText: 从数据库设计开始 →
actionLink: /database-design/
features:
- title: 平台层1.0.0
  details: 综合管控服务、视频流接入服务（结构化+交通事件）、视频流解析服务（结构化+交通事件）、结构化信息库服务
- title: 业务层
  details: 事件检测任务管理、实时告警、车辆检索、视频源管理、视频源分组管理、视频源标注
- title: 主要功能
  details: 车辆属性结构化；车牌识别；智能应急（行人&非机动车闯入高/快速路、车辆倒车逆行、路面烟火、团雾、异常停车、基于车辆计数的拥堵检测）
footer: Copyright © Sensetime
---

# SensePolaris 项目设计文档

- [maintainer guide](/guide/)
- [export to pdf](/guide/export-to-pdf/)
- [markdown cheatsheet](/polaris-fe/markdown/)