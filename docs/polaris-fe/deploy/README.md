


# 部署

## 打包部署
### 通过gitlab运行脚本自动打包部署
1. 将代码push到master分支或者develop分支上[gitlab代码仓库](https://gitlab.sz.sensetime.com/its-engineering/sensepolaris/sensepolaris-fe)
2. 进入代码仓库的[pipelines](https://gitlab.sz.sensetime.com/its-engineering/sensepolaris/sensepolaris-fe/pipelines) 模块  

3. 如果代码push到develop分支上只能部署到[92环境上](http://10.4.192.92:30888/#/login)。

<font color=#999AAA >如图所示（示例）：</font>

<img src="./images/92deploy.png" alt="手动点击部署到92环境" width="100%" height="200"/>

4. 如果代码push到master分支上，可以部署到[91环境上](http://10.4.192.91:30888/#/login)和[92环境上](http://10.4.192.92:30888/#/login)。

<font color="#999AAA" >如图所示（示例）：</font>

<img src="./images/91deploy.png" alt="91和92均可部署" width="100%" height="200"/>

5. 检查是否部署成功，查看build time是否正确。

<font color=#999AAA >如图所示（示例）：</font>

<img src="./images/buildtime.png" alt="build时间" width="100%" height="200"/>


### 手动通过docker打image部署

1. 首先安装docker 客户端。[具体安装流程请参考](https://www.runoob.com/docker/windows-docker-install.html)

2. 打开docker 客户端，不打开docker客户端会报如下错误。

<font color=#999AAA >错误提示（示例）：</font>

```text
The system cannot find the file specified. In the default daemon configuration on Windows,the docker client must be run elevated to connect. This error may also indicate that the docker daemon is not running.
```

3. 在项目文件下创建`.dockerignore` 文件，可以不参与`Dockerfile` 的构建。

<font color=#999AAA >代码如下（示例）：</font>

```javascript
.git
.vscode
dist
plopfile.js
node_modules
npm-debug.log
yarn-error.log
```

4. 在项目文件下创建`Dockerfile`文件，必须与`.dockerignore` 同级。

<font color=#999AAA >配置如下（示例）：</font>

```shell
# build environment
FROM registry.sensetime.com/its-pkg/node:10.19.0-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
RUN npm config set registry https://registry.npm.taobao.org
RUN npm install yarn --silent
RUN rm /app/package-lock.json
COPY package.json /app/package.json
RUN yarn --silent
COPY . /app
RUN yarn build

# production environment
FROM registry.sensetime.com/its-pkg/nginx:1.18.0-alpine
COPY --from=build /app/dist /usr/share/nginx/html
# RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
```

5. 在终端运行下面命令进行docker 镜像打包。

<font color=#999AAA >代码如下（示例）：</font>

```shell
# sensepolaris-fe:2.0.0-alpha 打包镜像名，可以随便改。
docker build -f Dockerfile -t sensepolaris-fe:2.0.0-alpha .
```
6. 登录docker

<font color=#999AAA >代码如下（示例）：</font>

```shell
docker login registry.sensetime.com // 使用ldap登录
```

7. 在项目中标记镜像 & 推送镜像到当前项目。

<font color=#999AAA >代码如下（示例）：</font>

```shell
docker tag sensepolaris-fe:2.0.0-alpha registry.sensetime.com/its-pkg/sensepolaris-fe:2.0.0-alpha;
docker image ls
docker tag 205ccd9bc79b registry.sensetime.com/its-pkg/sensepolaris-fe:2.0.0-alpha
```

<font color=#999AAA >如图所示（示例）：</font>

<img src="./images/localmirror.png" alt="build时间" width="100%" />

8. 推送镜像到当前项目。

<font color=#999AAA >代码如下（示例）：</font>

```shell
# its-pkg/sensepolaris-fe 是镜像仓库的路径所在的文件夹，如果这个路径乱写，会根据乱写的创建一个新的路径。
docker push registry.sensetime.com/its-pkg/sensepolaris-fe:2.0.0-alpha
```
9. update k8s 配置

 <font color=#999AAA >代码如下（示例）：</font>

```shell
ssh root@10.4.192.92
vi /etc/kubernetes/apps/polaris-frontend/frontend/pod.yml
```

<img src="./images/pod.png" alt="修改docker镜像文件路径" width="100%" />

该处修改docker镜像路径。

10. 重启k8s

<font color=#999AAA >代码如下（示例）：</font>

```shell
kubectl apply -f /etc/kubernetes/apps/polaris-frontend/frontend/pod.yml
```

## 文件配置
### nginx配置

在项目目录下创建 `nginx.conf`

1. 设置工作进程数,默认为1。

```shell
worker_processes 1;
```
2. 单个工作进程可以允许同时建立外部连接的数量，数字越大，能同时处理的连接越多，默认为1024。

```shell
events {
  worker_connections 1024;
}
```
3. 负载均衡配置

```shell
# weight:weight 越大，负载的权重就越大。
# fail_timeout:max_fails次失败后，暂停的时间。
# keeplive:最大保持的长连接数。
upstream zuul {
    server 10.4.192.92:30888 weight=2 fail_timeout=20s;
    keepalive 3000;
  }

  upstream stomp {
    server 10.4.192.92:30889 weight=2 fail_timeout=20s;
    keepalive 3000;
  }

  upstream rtspWSHost {
    server 10.4.192.92:10080 weight=2 fail_timeout=20s;
    keepalive 3000;
  }
```
```shell
server {
    # 监听的端口号
    listen 8080;
    # 服务名称 生产环境要修改成 公网ip 
    server_name localhost;
    # 配置默认的主页显示
    location / {
      root /usr/share/nginx/html;
      index index.html index.htm;
    }

    location /authorize {
      proxy_pass http://zuul;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;
      proxy_set_header Via "nginx";
    }

    location /uums {
      proxy_pass http://zuul;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;
      proxy_set_header Via "nginx";
    }

    location /v1 {
      proxy_pass http://zuul;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;
      proxy_set_header Via "nginx";
    }
    
    location /whale-push/stomp {
      proxy_pass http://stomp;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
    }

    location /rtsp-over-ws {
      proxy_pass http://rtspWSHost;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
    }

  }
```

4. 开启gzip压缩

```shell
  gzip on; #开启gzip
  gzip_min_length 1k; #低于1kb的资源不压缩
  gzip_comp_level 1; #压缩级别【1-9】，越大压缩率越高，同时消耗cpu资源也越多，建议设置在4左右。
  gzip_types application/json text/plain application/javascript application/x-javascript text/javascript text/xml text/css; #需要压缩哪些响应类型的资源，多个空格隔开
  gzip_vary on;# 是否在http header中添加vary: Accept-Encoding
```

### config.js 配置

1. 在项目public文件夹下创建config.js文件用于暴露给运维人员。

<font color=#999AAA >代码如下（示例）：</font>

```typescript
window.config = {
  title: '商汤睿途智慧交通平台',
  initialLocation: [114.264848, 30.620543], // 初始经纬度,从https://api.map.baidu.com/lbsapi/getpoint/index.html 拾取
  showVehicleType: true, // 车辆类型 轿车/厢式货车 etc...
  showVehicleModel: true, // 车辆型号 大众/丰田 etc...
  specialVehicle: false, // 智能检索/机动车类型中，是否开启特种车检索 关闭 false  开启 true  默认false
  keepTime: 5000, // 拥堵解除时间
  replayTimeDuration: 10000, // 回放视频长度(前后五秒,共十秒),单位毫秒,默认10000
  offline: false, // 是否开启离线底图模式,需先部署离线地图
  alarmUpdateTime: 60000, // 监控面板 今日告警，今日拥堵，设备数量，任务数量，每分钟刷新一次
  labellingMethod: 'manual', // automated': 自动; 'manual': 手动
  riderModule: false, // 智能检索中心，骑手模块手否显示 默认false
  taskTypes: [
    /* 交通事件 TRAFFIC_INCIDENT */
    'CONGESTION', // 拥堵
    'HAZE_EVENT', // 团雾
    'INTRUDER_ROAD', // 行人/非机动车闯入
    'ABNORMAL_PARKING', // 异常停车
    'LITTER_DROPPING', // 车辆抛洒
    'SMOKE_FIRE', // 烟火
    'WRONG_DIRECTION', // 倒逆行
    /* 机动车违法：ILLEGAL_VEHICLE */
    'ILLEGAL_CHANGE', // 违法变道
    'MULTIPLE_CHANGE', // 连续变道
    // 'NO_TURN_LIGHT', // 变道不打转向灯
    // 'ILLEGAL_PARKING', // 违章停车
    'VEHICLE_ILLEGAL_DIRECTION', // 倒车逆行
    /* 骑手/行人违法：ILLEGAL_RIDER */
    'RIDER_ILLEGAL_DIRECTION', // 骑手逆行
    'RIDER_OVERLOAD', // 骑手违法载人
    // 'RIDER_RED_LIGHT', // 骑手/行人闯红灯
    'RIDER_INTRUDER_ROAD', // 骑手闯禁区
    'NO_HELMET', // 骑手未戴头盔
  ],
};
```

2. 再src目录下创建insideConfig.ts，用于项目内部维护的配置。

<font color=#999AAA >代码如下（示例）：</font>

```typescript
const isDev = !!(process.env.NODE_ENV === 'development');

type env = 'development' | 'production';

const showPre: env[] = ['development'];

export default {
  showVehicleModel: false, // 车辆型号 大众/丰田 etc...
  topic: ['illegal_cyclist', 'illegal_vehicle', 'alarm-topic'],
  offlineHome: `${location.origin}/static/offlineMap/`,
  offlineTilesDir: `${location.origin}/static/offlineMap/darkTiles`,
  minZoom: 5,
  maxZoom: 18,
  zoom: 15,
  showPre,
  enableDevtoolsInProd: false, // 生产环境打开vue tooles
  // showVehicleModel: false, // 车辆型号 大众/丰田 etc...
  rtspWSHost: isDev
    ? 'ws://10.4.192.92:30888/rtsp-over-ws'
    : `ws://${location.origin.split('//')[1]}/rtsp-over-ws`,
  stomp: isDev
    ? 'http://10.4.192.92:30888/whale-push/stomp'
    : `${location.origin}/whale-push/stomp`,
  videoInterval: 5000, // 视频源轮询间隔
};
```
### 线上环境修改nginx配置和config.js
1. 登录上线服务器。

<font color=#999AAA >代码如下（示例）：</font>

```shell
ssh root@10.4.192.92
```
2. 进入config.yml文件。

<font color=#999AAA >代码如下（示例）：</font>

```typescript
cd /etc/kubernetes/apps/polaris-frontend/frontend/
ls
vim config.yml
# 或者直接进入
vim /etc/kubernetes/apps/polaris-frontend/frontend/config.yml
```
3. 重启k8s 

<font color=#999AAA >代码如下（示例）：</font>

```typescript
kubectl delete -f config.yml;kubectl apply -f config.yml
```
## 参考文献

```text
https://mherman.org/blog/dockerizing-a-vue-app/
https://www.ruanyifeng.com/blog/2018/02/nginx-docker.html
https://www.ruanyifeng.com/blog/2018/02/nginx-docker.html
```