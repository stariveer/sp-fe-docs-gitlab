

# 技术栈

# 前端架构解析

## 概述

基于 node.js、webpack、vue 和 element-ui 的前端开发开发框架

前后端分离:

- 单页面应用，支持局部刷新和前端路由
- 通过 restful 接口/websocket 实现前后端通信
- 前端项目单独打包部署

---

## 技术栈-语言

### HTML5

<span class="logo">![](./html5.jpg)</span>

- HTML5 是 HTML 最新的修订版本
- HTML5 添加了许多新的语法特征，更好的多媒体支持，包括`<video>`、`<audio>`和`<canvas>`元素，同时集成了 SVG 内容
- 除了少数历史版本浏览器不支持，html5 已成为包括移动端 web 开发在内的事实标准

### CSS3

<span class="logo">![](./css3.jpg)</span>

- CSS3 是 CSS(层叠样式表 Cascading StyleSheet)技术的升级版本
- CSS3 是在 CSS 的基础上上增加了很多新的特性，如新的盒子模型 flex-box 等。与低版本的 CSS 并不冲突
- 同 html5 一样，除了少数历史版本浏览器不支持，css3 已成为包括移动端 web 开发在内的事实标准

### Node.js

<span class="logo">![](./node.jpg)</span>

- 能够在服务器端运行 JavaScript 的开放源代码、跨平台 JavaScript 运行环境
- 本项目主要用来运行开发环境的 web server，以及 webpack

---

## 技术栈-frameworks

### **webpack**

<span class="logo">![](./006tNbRwly1fx689vtj2pj305k0680t2.jpg)</span>

- 基于 node.js 的开源前端打包工具
- 提供了前端开发缺乏的模块化开发方式，使用加载器(loader)来将资源转化成模块，并从它生成优化过的代码

### **vue.js** - 前端 mvvm(Model-View-ViewModel) 库

<span class="logo">![](./006tNbRwgy1fx6atfiz3yj3046046mwx.jpg)</span>

- 提供数据驱动界面的能力，摆脱 dom 操作的桎梏
- 组件化开发的思路，提高开发效率和规范化程度

### **vue-router** - Vue.js 官方的路由管理器

- 嵌套的路由/视图表
- 模块化的、基于组件的路由配置
- 路由参数、查询、通配符
- 基于 Vue.js 过渡系统的视图过渡效果
- 细粒度的导航控制
- 带有自动激活的 CSS class 的链接
- HTML5 历史模式或 hash 模式，在 IE9 中自动降级
- 自定义的滚动条行为


### **Vuex** - 专为 Vue.js 应用程序开发的状态管理模式

- 集中式存储管理应用的所有组件的状态
- 以相应的规则保证状态以一种可预测的方式发生变化


### Element-ui

<span class="logo">![](./006tNbRwly1fx6608dvcxj3042012wee.jpg)</span>

- 基于 vue.js 的网站快速成型工具
- 功能强大，体验优秀，支持配置
- 利于全栈 UI 风格统一

典型表格:

<span class="img p50">![](./006tNbRwly1fx661wkjtkj31d60m2ju7.jpg)</span>

典型表单:

包括各种表单项，比如输入框、选择器、开关、单选框、多选框等。

<span class="img p50">![](./006tNbRwly1fx665btvddj31cs10gn0i.jpg)</span>

TimePicker 时间选择器、Dialog 对话框、Pagination 分页、Upload 上传、Breadcrumb 面包屑 etc...


<style>
.logo{
  height: 100px;
  max-height: 100px;
  display: inline-block;
}
.logo img{
  max-height: 100%;
}
.img.p50{
  display: inline-block;
  width: 50%;
}
.img.p50 img{
  max-width: 100%;
}
</style>
