

# SensePolaris-FE 开发手册

## 安装依赖

### 安装node环境
前往node官网选择你需要的系统的版本
- [Node.js](https://nodejs.org/en/download/)
```bash
#查看node.js是否安装完成
node -v
```
### 利用npm安装yarn 利用yarn安装项目所需依赖
```bash

# install yarn 安装yarn
npm i yarn -g

# install dependencies 利用yarn安装项目依赖
yarn
```
### 安装项目UI组件razor
- [Razor](http://10.111.32.60/razor)
```js
git pull
git submodule init
git submodule update

// for 1st time
git submodule add -b "master" git@gitlab.sz.sensetime.com:its-engineering/sensepolaris/razor.git razor
```

### yarn启动本地项目、打包项目
```bash
# start dev 启动本地服务
yarn start

# build production 打包项目
yarn build
```

## Windmill使用以及路由配置

### Windmill介绍
* [Windmill](http://10.111.32.60/windmill)

### Windmill新增模块以及页面
- 命令行新建
```bash
# add new module 新增模块
yarn new module

# add new page 在已有模块新增页面
yarn new page
```
- 手动新建

`src/modules` 目录存放着项目的所有模块，在此目录下新建目录

<span class="img p50">![](./addModules.png)</span>

### Windmill 命名规则
- WindmillMeta module name, 文件夹名称: 使用**全大写\_下划线**

```js
const meta: WindmillMeta = {
  name: 'JUST_SAMPLE',
  route,
  store,
};
```

- vue 公共 module name, class name: 使用**首字母大写的驼峰**

```js
export default class NavTop extends Vue {
```

- 路由路径: **全小写-连字符**,route name: **驼峰**

```js
route: {
  path: '/docs/icon-page', // 全小写-连字符
  name: 'docsIcon', // 驼峰
  component: () => import('./pages/Icon/index.vue'),
},
```

英语单词尽量不要使用复数(带"s")
