

# POLARIS RESTFul API


**简介**:POLARIS RESTFul API


**HOST**:10.4.192.92:30884


**联系人**:


**Version**:1.1.0


**接口路径**:http://10.4.192.92:30884/v1/doc.html#/home


[TOC]






# alarm-controller


## 查询告警记录数量


**接口地址**:`/v1/alarm/count`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:查询告警记录数量接口


**请求示例**:


```javascript
{
	"endTime": 0,
	"originAlarmNo": "",
	"startTime": 0,
	"status": "",
	"taskName": "",
	"taskSerial": "",
	"taskType": "",
	"valid": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|AlarmCountRequest|AlarmCountRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||false|integer(int64)||
|originAlarmNo|起始告警编号||false|string||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||false|integer(int64)||
|status|告警状态,可用值:START,CONTINUE,STOP||false|string||
|taskName|任务名称||false|string||
|taskSerial|任务编号||false|string||
|taskType|任务名称,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE||false|string||
|valid|是否有效,  e.g. 0-无效 1-有效 2-未研判||false|integer(int32)||
|videoSerials|视频源序号列表||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|AlarmCountResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|count|记录数量|integer(int64)|integer(int64)|


**响应示例**:
```javascript
{
	"count": 0
}
```


## 查询告警列表


**接口地址**:`/v1/alarm/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:查询告警列表接口


**请求示例**:


```javascript
{
	"endTime": 0,
	"originAlarmNo": "",
	"pageNum": 1,
	"pageSize": 20,
	"startTime": 0,
	"status": "",
	"taskName": "",
	"taskSerial": "",
	"taskType": "",
	"valid": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|AlarmListRequest|AlarmListRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||false|integer(int64)||
|originAlarmNo|起始告警编号||false|string||
|pageNum|第几页||false|integer(int32)||
|pageSize|每页记录数||false|integer(int32)||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||false|integer(int64)||
|status|告警状态,可用值:START,CONTINUE,STOP||false|string||
|taskName|任务名称||false|string||
|taskSerial|任务编号||false|string||
|taskType|任务名称,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE||false|string||
|valid|是否有效,  e.g. 0-无效 1-有效 2-未研判||false|integer(int32)||
|videoSerials|视频源序号列表||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfAlarmListResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|AlarmListResponse|AlarmListResponse|
|entries|告警列表|array|TrafficAlarm|
|alarmNo|告警编号||false|string||
|alarmTime|告警时间||false|integer(int64)||
|anomalyObject|异常停车告警详细信息||false|AnomalyObject|AnomalyObject|
|startTime|异常停车开始时间||false|integer(int64)||
|stopTime|异常停车结束时间||false|integer(int64)||
|capture|抓拍图片信息||false|Capture|Capture|
|height|大图高度||false|integer(int32)||
|imageUrl|抓拍图片url||false|string||
|time|抓拍时间, 单位：ms||false|integer(int64)||
|vertices|小图坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|width|大图宽度||false|integer(int32)||
|congestionObject|拥堵告警详细信息||false|CongestionObject|CongestionObject|
|avgCount|一分钟内发生拥堵对应观察区的车辆平均数量||false|number(float)||
|maxCount|一分钟内发生拥堵对应观察区的车辆最大数量||false|integer(int64)||
|positions|拥堵观察区坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|roadMaskCode|拥堵观察区的哈希码||false|integer(int32)||
|threshold|拥堵告警阈值||false|integer(int64)||
|duration|告警时长||false|number(double)||
|hazeEventObject|团雾告警详细信息||false|HazeEventObject|HazeEventObject|
|level|||false|integer(int32)||
|originAlarmNo|起始告警编号||false|string||
|status|告警状态,可用值:START,CONTINUE,STOP||false|string||
|task|告警摄像机||false|TrafficTask|TrafficTask|
|taskName|任务名称||false|string||
|taskSerial|任务编号||false|string||
|taskType|任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE,LABELLING_AUTOMATED||false|string||
|valid|是否真实有效||false|integer(int32)||
|vehicle|车辆属性信息||false|Vehicle|Vehicle|
|color|可用值:COLOR_TYPE_NONE,BLACK,WHITE,GRAY,RED,BLUE,YELLOW,ORANGE,BROWN,GREEN,PURPLE,CRAN,PINK,TRANSPARENT,COLOR_TYPE_OTHER||false|string||
|model|||false|string||
|plateNo|||false|string||
|type|可用值:ST_AUTOMOBILE_TYPE_NONE,ST_CAR,ST_VAN,ST_SMALL_TRUCK,ST_BIG_TRUCK,ST_SUV,ST_BIG_BUS,ST_MED_BUS,ST_AUTOMOBILE_TYPE_OTHER,ST_HUMAN_POWERED_VEHICLE_TYPE_NONE,ST_MOTOR,ST_EBIKE,ST_BICYCLE,ST_TRICYCLE,ST_HUMAN_POWERED_VEHICLE_TYPE_OTHER||false|string||
|video|告警摄像机||false|Video|Video|
|labelStatus|标注状态：0(未标注), 1(已标注)||false|boolean||
|name|摄像头名称||false|string||
|serial|摄像头编号||false|string||
|wrongDirectionObject|逆行告警详细信息||false|WrongDirectionObject|WrongDirectionObject|
|originTrails|原始的轨迹||false|array|TrailPositionPoint|
|capturedTime|抓拍时间||false|integer(int64)||
|keyPoints|车辆关键点 逆时针顺序 司机视角的左前轮 左后轮 右后轮 右前轮||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|keyPointsScores|车辆关键点置信度||false|array|number|
|smoothTrails|平滑后的轨迹||false|array|SmoothTrail|
|captureTime|抓拍时间  毫秒数||false|integer(int64)||
|vertex|平滑后的关键点坐标||false|Vertex|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"entries": [
			{
				"alarmNo": "",
				"alarmTime": 0,
				"anomalyObject": {
					"startTime": 0,
					"stopTime": 0
				},
				"capture": {
					"height": 0,
					"imageUrl": "",
					"time": 0,
					"vertices": [
						{
							"x": 0,
							"y": 0
						}
					],
					"width": 0
				},
				"congestionObject": {
					"avgCount": 0,
					"maxCount": 0,
					"positions": [
						{
							"x": 0,
							"y": 0
						}
					],
					"roadMaskCode": 0,
					"threshold": 0
				},
				"duration": 0,
				"hazeEventObject": {
					"level": 0
				},
				"originAlarmNo": "",
				"status": "",
				"task": {
					"taskName": "",
					"taskSerial": "",
					"taskType": ""
				},
				"valid": 0,
				"vehicle": {
					"color": "",
					"model": "",
					"plateNo": "",
					"type": ""
				},
				"video": {
					"labelStatus": true,
					"name": "",
					"serial": ""
				},
				"wrongDirectionObject": {
					"originTrails": [
						{
							"capturedTime": 0,
							"keyPoints": [
								{
									"x": 0,
									"y": 0
								}
							],
							"keyPointsScores": []
						}
					],
					"smoothTrails": [
						{
							"captureTime": 0,
							"vertex": {
								"x": 0,
								"y": 0
							}
						}
					]
				}
			}
		],
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查看告警详情


**接口地址**:`/v1/alarm/{alarmNo}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:查看告警详情


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|alarmNo|告警编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|TrafficAlarm|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|alarmNo|告警编号|string||
|alarmTime|告警时间|integer(int64)|integer(int64)|
|anomalyObject|异常停车告警详细信息|AnomalyObject|AnomalyObject|
|startTime|异常停车开始时间|integer(int64)||
|stopTime|异常停车结束时间|integer(int64)||
|capture|抓拍图片信息|Capture|Capture|
|height|大图高度|integer(int32)||
|imageUrl|抓拍图片url|string||
|time|抓拍时间, 单位：ms|integer(int64)||
|vertices|小图坐标|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|width|大图宽度|integer(int32)||
|congestionObject|拥堵告警详细信息|CongestionObject|CongestionObject|
|avgCount|一分钟内发生拥堵对应观察区的车辆平均数量|number(float)||
|maxCount|一分钟内发生拥堵对应观察区的车辆最大数量|integer(int64)||
|positions|拥堵观察区坐标|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|roadMaskCode|拥堵观察区的哈希码|integer(int32)||
|threshold|拥堵告警阈值|integer(int64)||
|duration|告警时长|number(double)|number(double)|
|hazeEventObject|团雾告警详细信息|HazeEventObject|HazeEventObject|
|level||integer(int32)||
|originAlarmNo|起始告警编号|string||
|status|告警状态,可用值:START,CONTINUE,STOP|string||
|task|告警摄像机|TrafficTask|TrafficTask|
|taskName|任务名称|string||
|taskSerial|任务编号|string||
|taskType|任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE,LABELLING_AUTOMATED|string||
|valid|是否真实有效|integer(int32)|integer(int32)|
|vehicle|车辆属性信息|Vehicle|Vehicle|
|color|可用值:COLOR_TYPE_NONE,BLACK,WHITE,GRAY,RED,BLUE,YELLOW,ORANGE,BROWN,GREEN,PURPLE,CRAN,PINK,TRANSPARENT,COLOR_TYPE_OTHER|string||
|model||string||
|plateNo||string||
|type|可用值:ST_AUTOMOBILE_TYPE_NONE,ST_CAR,ST_VAN,ST_SMALL_TRUCK,ST_BIG_TRUCK,ST_SUV,ST_BIG_BUS,ST_MED_BUS,ST_AUTOMOBILE_TYPE_OTHER,ST_HUMAN_POWERED_VEHICLE_TYPE_NONE,ST_MOTOR,ST_EBIKE,ST_BICYCLE,ST_TRICYCLE,ST_HUMAN_POWERED_VEHICLE_TYPE_OTHER|string||
|video|告警摄像机|Video|Video|
|labelStatus|标注状态：0(未标注), 1(已标注)|boolean||
|name|摄像头名称|string||
|serial|摄像头编号|string||
|wrongDirectionObject|逆行告警详细信息|WrongDirectionObject|WrongDirectionObject|
|originTrails|原始的轨迹|array|TrailPositionPoint|
|capturedTime|抓拍时间||false|integer(int64)||
|keyPoints|车辆关键点 逆时针顺序 司机视角的左前轮 左后轮 右后轮 右前轮||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|keyPointsScores|车辆关键点置信度||false|array|number|
|smoothTrails|平滑后的轨迹|array|SmoothTrail|
|captureTime|抓拍时间  毫秒数||false|integer(int64)||
|vertex|平滑后的关键点坐标||false|Vertex|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||


**响应示例**:
```javascript
{
	"alarmNo": "",
	"alarmTime": 0,
	"anomalyObject": {
		"startTime": 0,
		"stopTime": 0
	},
	"capture": {
		"height": 0,
		"imageUrl": "",
		"time": 0,
		"vertices": [
			{
				"x": 0,
				"y": 0
			}
		],
		"width": 0
	},
	"congestionObject": {
		"avgCount": 0,
		"maxCount": 0,
		"positions": [
			{
				"x": 0,
				"y": 0
			}
		],
		"roadMaskCode": 0,
		"threshold": 0
	},
	"duration": 0,
	"hazeEventObject": {
		"level": 0
	},
	"originAlarmNo": "",
	"status": "",
	"task": {
		"taskName": "",
		"taskSerial": "",
		"taskType": ""
	},
	"valid": 0,
	"vehicle": {
		"color": "",
		"model": "",
		"plateNo": "",
		"type": ""
	},
	"video": {
		"labelStatus": true,
		"name": "",
		"serial": ""
	},
	"wrongDirectionObject": {
		"originTrails": [
			{
				"capturedTime": 0,
				"keyPoints": [
					{
						"x": 0,
						"y": 0
					}
				],
				"keyPointsScores": []
			}
		],
		"smoothTrails": [
			{
				"captureTime": 0,
				"vertex": {
					"x": 0,
					"y": 0
				}
			}
		]
	}
}
```


## 获取告警热力图


**接口地址**:`/v1/alarm/{alarmNo}/heatmap`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:获取告警热力图接口


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|alarmNo|告警编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|HeatMapList|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|heatMapList||array|HeatMap|
|value|权重|number(float)||
|x|x坐标|integer(int32)||
|y|y坐标|integer(int32)||


**响应示例**:
```javascript
{
	"heatMapList": [
		{
			"value": 0,
			"x": 0,
			"y": 0
		}
	]
}
```


## 告警研判


**接口地址**:`/v1/alarm/{alarmNo}/valid/{value}`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:告警研判接口


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|alarmNo|告警编号|path|true|string||
|value|研判结果，0-无效， 1-有效， 2-未研判|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|WildcardType|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|present||boolean||


**响应示例**:
```javascript
{
	"present": true
}
```


# cyclist-controller


## 获取指定条件的骑手抓拍计数


**接口地址**:`/v1/cyclist/count`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:获取指定条件的骑手抓拍计数。


**请求示例**:


```javascript
{
	"endTime": 0,
	"filters": {},
	"startTime": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|根据时空属性过滤非机动车请求|body|true|PoweredTimeQueryRequest|PoweredTimeQueryRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|filters|过滤条件。  \n注意车辆类型、佩戴头盔。  \n样例如下：  \n```json   \n{  \n    'st_vehicle_class': ['']  车辆类型 ref PoweredsCaptureVo.huamanPoweredVehicleClass\n    'helmet_style': ['']  佩戴头盔  ref  PoweredsCaptureVo.helmetStyle\n}  \n```||false|object||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|videoSerials|选择的视频源serials。  \n注意：whale-search-vehicle服务不会对视频源可用性进行校验。在传入之前，使用视频源服务来验证视频源的可用性。  \n在whale-search-vehicle中，该值不接受空值。||true|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTimeSpaceCountResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TimeSpaceCountResponse|TimeSpaceCountResponse|
|capturesCount|返回符合当前条件的抓拍的总数。|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"capturesCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 根据指定条件过滤骑手抓拍


**接口地址**:`/v1/cyclist/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:根据指定条件过滤骑手抓拍。  



**请求示例**:


```javascript
{
	"endTime": 0,
	"filters": {},
	"pageNum": 1,
	"pageSize": 20,
	"startTime": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|根据时空属性过滤非机动车请求|body|true|PoweredTimeSpaceQueryRequest|PoweredTimeSpaceQueryRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|filters|过滤条件。  \n注意车辆类型、佩戴头盔。  \n样例如下：  \n```json   \n{  \n    'st_vehicle_class': ['']  车辆类型 \n						 ref 摩托车：st_vehicle_class: ["MOTOR"] \n						电动车：st_vehicle_class: ["EBIKE"] \n						自行车：st_vehicle_class: ["BICYCLE"] \n						三轮车：st_vehicle_class: ["TRICYCLE"]\n    'helmet_style': ['']  佩戴头盔  ref  未戴头盔：helmetStyle: ["UNKNOWN"] 戴头盔：helmetStyle: ["HELMET"]\n}  \n```||false|object||
|pageNum|第几页||false|integer(int32)||
|pageSize|每页记录数||false|integer(int32)||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|videoSerials|选择的视频源serials。  \n注意：whale-search-vehicle服务不会对视频源可用性进行校验。在传入之前，使用视频源服务来验证视频源的可用性。  \n在whale-search-vehicle中，该值不接受空值。||true|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfPoweredTimeSpaceQueryResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|PoweredTimeSpaceQueryResponse|PoweredTimeSpaceQueryResponse|
|cyclists|车辆抓拍列表。  \n注意：由于视频源缓存的原因，当修改一个视频源的信息后，这里可能不会立刻刷新，会存在最坏10分钟的延迟。|array|PoweredsCaptureVo|
|address|地址||false|string||
|bigImageUrl|大图URL||false|string||
|captureTime|抓拍时间||false|integer(int64)||
|helmetStyle|头盔佩戴，可能的取值：  \n * ```UNKNOWN``` ：未带头盔  \n * ```HELMET``` ：带头盔 \n||false|string||
|huamanPoweredVehicleClass|车辆类型信息，可能的值: \n * ```MOTOR``` ：摩托车  \n * ```EBIKE``` ：电动车 \n * ```BICYCLE``` ：自行车  \n * ```TRICYCLE``` ：三轮车  \n||false|string||
|humanPoweredVehicleColor|车辆颜色。可能的取值：  \n * ```UNKNOWN``` ：未知  \n * ```OTHER``` ：其他  \n * ```BLACK``` ：黑  \n * ```WHITE``` ：白  \n * ```GRAY``` ：灰  \n * ```RED``` ：红  \n * ```BLUE``` ：蓝  \n * ```YELLOW``` ：黄  \n * ```ORANGE``` ：橙  \n * ```BROWN``` ：棕  \n * ```GREEN``` ：绿  \n * ```PURPLE``` ：紫  \n * ```CYAN``` ：青  \n * ```PINK``` ：粉  \n||false|string||
|imageUrl|小图URL||false|string||
|latitude|纬度||false|number(float)||
|longitude|经度||false|number(float)||
|objectPosition|目标位置||false|PoweredPositionVo|PoweredPositionVo|
|height|高度||false|integer(int32)||
|vertices|车辆坐标||false|array|VertexVo|
|x|x 轴坐标||false|integer(int32)||
|y|y 轴坐标||false|integer(int32)||
|width|宽度||false|integer(int32)||
|plateNo|车牌信息。可能会为空或者不存在，如拍摄图片为车辆侧面时。||false|string||
|resourceTypeId|视频源类型||false|integer(int64)||
|videoName|视频源名称||false|string||
|videoSerial|视频源Serial||false|string||
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"cyclists": [
			{
				"address": "",
				"bigImageUrl": "",
				"captureTime": 0,
				"helmetStyle": "",
				"huamanPoweredVehicleClass": "",
				"humanPoweredVehicleColor": "",
				"imageUrl": "",
				"latitude": 0,
				"longitude": 0,
				"objectPosition": {
					"height": 0,
					"vertices": [
						{
							"x": 0,
							"y": 0
						}
					],
					"width": 0
				},
				"plateNo": "",
				"resourceTypeId": 0,
				"videoName": "",
				"videoSerial": ""
			}
		],
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# dashboard-controller


## 查询今日告警地点排名


**接口地址**:`/v1/dashboard/alarm-rank-today`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:polaris-dashboard查询今日告警地点排名


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|topK|排名topK|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfListAlarmItemResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|ListAlarmItemResponse|ListAlarmItemResponse|
|alarmItems|告警信息|array|AlarmItem|
|count|告警数||false|integer(int32)||
|duration|持续时长||false|number(double)||
|rank|排名||false|integer(int32)||
|video|摄像机信息||false|Video|Video|
|labelStatus|标注状态：0(未标注), 1(已标注)||false|boolean||
|name|摄像头名称||false|string||
|serial|摄像头编号||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"alarmItems": [
			{
				"count": 0,
				"duration": 0,
				"rank": 0,
				"video": {
					"labelStatus": true,
					"name": "",
					"serial": ""
				}
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查询今日告警总数


**接口地址**:`/v1/dashboard/alarm-total-today`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:polaris-dashboard查询今日告警总数


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查询今日拥堵时长排名


**接口地址**:`/v1/dashboard/congestion-rank-today`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:polaris-dashboard查询今日拥堵时长排名


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|topK|排名topK|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfListAlarmItemResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|ListAlarmItemResponse|ListAlarmItemResponse|
|alarmItems|告警信息|array|AlarmItem|
|count|告警数||false|integer(int32)||
|duration|持续时长||false|number(double)||
|rank|排名||false|integer(int32)||
|video|摄像机信息||false|Video|Video|
|labelStatus|标注状态：0(未标注), 1(已标注)||false|boolean||
|name|摄像头名称||false|string||
|serial|摄像头编号||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"alarmItems": [
			{
				"count": 0,
				"duration": 0,
				"rank": 0,
				"video": {
					"labelStatus": true,
					"name": "",
					"serial": ""
				}
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查询任务运行情况


**接口地址**:`/v1/dashboard/task-status`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:polaris-dashboard查询任务运行情况


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTaskStatistics|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TaskStatistics|TaskStatistics|
|abnormalCount|异常数|integer(int32)||
|runningCount|运行数|integer(int32)||
|terminalCount|终止数|integer(int32)||
|total|总数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"abnormalCount": 0,
		"runningCount": 0,
		"terminalCount": 0,
		"total": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# task-controller


## 创建智能应急任务


**接口地址**:`/v1/task`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:创建智能应急任务


**请求示例**:


```javascript
{
	"effectiveDuration": {
		"endDay": "",
		"startDay": ""
	},
	"longRun": true,
	"parkingDurationThreshold": 0,
	"remark": "",
	"taskName": "",
	"taskScenario": "",
	"taskType": "",
	"taskWorkingTimes": [
		{
			"end": "",
			"start": ""
		}
	],
	"theWholeDay": true,
	"videos": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|CreateTaskRequest|CreateTaskRequest|
|effectiveDuration|当longRun = false，需要指定任务有效日期||false|EffectiveDuration|EffectiveDuration|
|endDay|结束日期, 时间戳格式||false|string||
|startDay|开始日期, 时间戳格式||false|string||
|longRun|长期有效时，该值为true；否则为false，此时关注startDay和endDay||true|boolean||
|parkingDurationThreshold|停车时间阈值。||false|integer(int32)||
|remark|备注||false|string||
|taskName|任务名称||true|string||
|taskScenario|任务场景,可用值:CITY,HIGHWAY||false|string||
|taskType|任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE||true|string||
|taskWorkingTimes|当theWholeDay = false，需要指定任务工作的时间段||false|array|TaskWorkingTime|
|end|结束工作时间,形如hh:mm:ss||false|string||
|start|开始工作时间,形如hh:mm:ss||false|string||
|theWholeDay|该任务是否处于运行状态，此字段用于调度任务||true|boolean||
|videos|任务关联的摄像头||true|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|WildcardType|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|present||boolean||


**响应示例**:
```javascript
{
	"present": true
}
```


## 获取智能应急任务列表


**接口地址**:`/v1/task/list`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:获取智能应急任务列表


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|name|智能应急任务名称|query|false|string||
|pageNum|第几页|query|false|integer(int32)||
|pageSize|分页大小|query|false|integer(int32)||
|status|智能应急任务状态,可用值:RUNNING,ABNORMAL,TERMINAL|query|false|string||
|type|智能应急任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTaskListResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TaskListResponse|TaskListResponse|
|entries|任务列表|array|TaskListEntry|
|abnormalVideos|异常的视频||false|array|AbnormalVideo|
|name|摄像头名称||false|string||
|videoSerial|摄像头编号||false|string||
|alarmCount|告警数||false|integer(int32)||
|name|任务名称||false|string||
|normalVideoCount|正常的视频数量||false|integer(int32)||
|status|任务状态,可用值:RUNNING,ABNORMAL,TERMINAL||false|string||
|taskSerial|任务编号||false|string||
|totalVideoCount|视频总数||false|integer(int32)||
|type|任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE||false|string||
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"entries": [
			{
				"abnormalVideos": [
					{
						"name": "",
						"videoSerial": ""
					}
				],
				"alarmCount": 10,
				"name": "",
				"normalVideoCount": 10,
				"status": "",
				"taskSerial": "",
				"totalVideoCount": 10,
				"type": ""
			}
		],
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查看智能应急任务详情


**接口地址**:`/v1/task/{taskSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:查看智能应急任务详情


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|taskSerial|任务编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTaskDetailResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TaskDetailResponse|TaskDetailResponse|
|effectiveDuration|当longRun = false，需要指定任务有效日期|EffectiveDuration|EffectiveDuration|
|endDay|结束日期, 时间戳格式||false|string||
|startDay|开始日期, 时间戳格式||false|string||
|longRun|长期有效时，该值为1；否则为0，此时关注startDay和endDay|boolean||
|parkingDurationThreshold|停车时间阈值|integer(int32)||
|remark|备注|string||
|status|任务状态,可用值:RUNNING,ABNORMAL,TERMINAL|string||
|taskName|任务名称|string||
|taskScenario|任务场景,可用值:CITY,HIGHWAY|string||
|taskSerial|任务编号|string||
|taskType|任务类型,可用值:CONGESTION,LITTER_DROPPING,INTRUDER_ROAD,WRONG_DIRECTION,ILLEGAL_PARKING,HAZE_EVENT,SMOKE_FIRE|string||
|taskWorkingTimes|当theWholeDay = false，需要指定任务工作的时间段|array|TaskWorkingTime|
|end|结束工作时间,形如hh:mm:ss||false|string||
|start|开始工作时间,形如hh:mm:ss||false|string||
|theWholeDay|该任务是否处于运行状态，此字段用于调度任务|boolean||
|videos|视频源编号，前端通过该编号获取详情|array|Video|
|labelStatus|标注状态：0(未标注), 1(已标注)||false|boolean||
|name|摄像头名称||false|string||
|serial|摄像头编号||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"effectiveDuration": {
			"endDay": "",
			"startDay": ""
		},
		"longRun": true,
		"parkingDurationThreshold": 0,
		"remark": "",
		"status": "",
		"taskName": "",
		"taskScenario": "",
		"taskSerial": "",
		"taskType": "",
		"taskWorkingTimes": [
			{
				"end": "",
				"start": ""
			}
		],
		"theWholeDay": true,
		"videos": [
			{
				"labelStatus": true,
				"name": "",
				"serial": ""
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 修改任务信息


**接口地址**:`/v1/task/{taskSerial}`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:修改任务信息


**请求示例**:


```javascript
{
	"deletedVideos": [],
	"effectiveDuration": {
		"endDay": "",
		"startDay": ""
	},
	"longRun": true,
	"newVideos": [],
	"parkingDurationThreshold": 0,
	"remark": "",
	"taskName": "",
	"taskWorkingTimes": [
		{
			"end": "",
			"start": ""
		}
	],
	"theWholeDay": true
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|任务参数|body|false|ModifyTaskInfoRequest|ModifyTaskInfoRequest|
|deletedVideos|原任务已关联的视频设备，改为之后不关联||false|array|string|
|effectiveDuration|当longRun = false，需要指定任务有效日期||false|EffectiveDuration|EffectiveDuration|
|endDay|结束日期, 时间戳格式||false|string||
|startDay|开始日期, 时间戳格式||false|string||
|longRun|长期有效时，该值为1；否则为0，此时关注startDay和endDay||true|boolean||
|newVideos|需要添加关联的视频设备||false|array|string|
|parkingDurationThreshold|停车时间阈值。||false|integer(int32)||
|remark|备注||false|string||
|taskName|任务名称||false|string||
|taskWorkingTimes|当theWholeDay = false，需要指定任务工作的时间段||false|array|TaskWorkingTime|
|end|结束工作时间,形如hh:mm:ss||false|string||
|start|开始工作时间,形如hh:mm:ss||false|string||
|theWholeDay|该任务是否处于运行状态，此字段用于调度任务||true|boolean||
|taskSerial|任务编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 设备重新上线


**接口地址**:`/v1/task/{taskSerial}/online/{videoSerial}`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:设备重新上线，只有当任务在运行时间段中，才能将设备重新上线


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|taskSerial|任务编号|path|false|string||
|videoSerial|设备编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 重启任务


**接口地址**:`/v1/task/{taskSerial}/restart`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:重启任务


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|taskSerial|任务编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 终止任务


**接口地址**:`/v1/task/{taskSerial}/terminal`


**请求方式**:`DELETE`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:终止任务


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|taskSerial|任务编号|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# vehicle-controller


## 获取指定条件的车辆抓拍计数


**接口地址**:`/v1/vehicle/count`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:获取指定条件的车辆抓拍计数。


**请求示例**:


```javascript
{
	"endTime": 0,
	"filters": {},
	"startTime": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|根据时空属性过滤车辆请求|body|true|VehicleTimeQueryRequest|VehicleTimeQueryRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|filters|过滤条件。  \n注意车牌、车辆型号、车辆品牌仅限一个。按照第一个识别。  \n样例如下：  \n```json   \n{  \n    'plate_no': ['']  车牌 \n    'plate_class': ['']   车牌类型  Ref VehicleCaptureVo.plateClass \n    'plate_color': ['']   车牌颜色  Ref VehicleCaptureVo.plateColor\n    'vehicle_brand': ['']  车辆品牌  \n    'vehicle_model': ['']  车辆型号 \n    'vehicle_class': ['']  车辆类型  Ref VehicleCaptureVo.vehicleClass\n    'vehicle_color': ['']  车辆颜色  Ref VehicleCaptureVo.vehicleColor\n    'special_class': ['']  特种车信息 Ref VehicleCaptureVo.specialAutomobileClass\n}  \n```||false|object||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|videoSerials|选择的视频源serials。  \n注意：whale-search-vehicle服务不会对视频源可用性进行校验。在传入之前，使用视频源服务来验证视频源的可用性。  \n在whale-search-vehicle中，该值不接受空值。||true|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTimeSpaceCountResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TimeSpaceCountResponse|TimeSpaceCountResponse|
|capturesCount|返回符合当前条件的抓拍的总数。|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"capturesCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 根据指定条件过滤车辆抓拍


**接口地址**:`/v1/vehicle/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:根据指定条件过滤车辆抓拍。  



**请求示例**:


```javascript
{
	"endTime": 0,
	"filters": {},
	"pageNum": 1,
	"pageSize": 20,
	"startTime": 0,
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|根据时空属性过滤车辆请求|body|true|VehicleTimeSpaceQueryRequest|VehicleTimeSpaceQueryRequest|
|endTime|结束时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|filters|过滤条件。  \n注意车牌、车辆型号、车辆品牌仅限一个。按照第一个识别。  \n样例如下：  \n```json   \n{  \n    'plate_no': ['']  车牌 \n    'plate_class': ['']   车牌类型  Ref VehicleCaptureVo.plateClass \n    'plate_color': ['']   车牌颜色  Ref VehicleCaptureVo.plateColor\n    'vehicle_brand': ['']  车辆品牌  \n    'vehicle_model': ['']  车辆型号 \n    'vehicle_color': ['']  车辆颜色  Ref VehicleCaptureVo.vehicleColor\n    'vehicle_class': ['']  车辆类型  \n						Ref 轿车:vehicle_class["CAR"] \n						小卡车:vehicle_class["SMALL_TRUCK"]\n						大卡车:vehicle_class["BIG_TRUCK"]\n						运动型多功能车:vehicle_class["SUV"]\n						大型公共汽车:vehicle_class["BIG_BUS"]\n						中型公共汽车:vehicle_class["MED_BUS"]\n						厢式货车（注意：实际测试发现别克GL8这类六座商务车也在这个类型里）:vehicle_class["VAN"]\n    'special_class': ['']  特种车信息 \n						Ref 普通车辆:special_class["COMMON_VEHICLE"]\n						救护车:special_class["AMBULANCE"]\n						消防车:special_class["FIRE_FIGHT"]\n						公检法车:special_class["PUBLIC_SECURITY"]\n						渣土车:special_class["DUMP"]\n						搅拌车:special_class["MIXER"]\n						出租车:special_class["TAXI"]\n}  \n```||false|object||
|pageNum|第几页||false|integer(int32)||
|pageSize|每页记录数||false|integer(int32)||
|startTime|开始时间。毫秒时间戳。不早于2018年6月1日。||true|integer(int64)||
|videoSerials|选择的视频源serials。  \n注意：whale-search-vehicle服务不会对视频源可用性进行校验。在传入之前，使用视频源服务来验证视频源的可用性。  \n在whale-search-vehicle中，该值不接受空值。||true|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVehicleTimeSpaceQueryResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VehicleTimeSpaceQueryResponse|VehicleTimeSpaceQueryResponse|
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|vehicles|车辆抓拍列表。  \n注意：由于视频源缓存的原因，当修改一个视频源的信息后，这里可能不会立刻刷新，会存在最坏10分钟的延迟。|array|VehicleCaptureVo|
|address|地址||false|string||
|bigImageUrl|大图URL||false|string||
|captureTime|抓拍时间||false|integer(int64)||
|imageUrl|小图URL||false|string||
|latitude|纬度||false|number(float)||
|longitude|经度||false|number(float)||
|objectPosition|目标位置||false|VehiclePositionVo|VehiclePositionVo|
|height|高度||false|integer(int32)||
|vertices|车辆坐标||false|array|VertexVo|
|x|x 轴坐标||false|integer(int32)||
|y|y 轴坐标||false|integer(int32)||
|width|宽度||false|integer(int32)||
|plateClass|车牌类型。可能的取值：  \n * ```LARGE_VEHICLE``` ：大型汽车号牌  \n * ```SMALL_VEHICLE``` ：小型汽车号牌  \n * ```EMBASSY_VEHICLE``` ：使馆汽车号牌  \n * ```CONSULAR_VEHICLE``` ：领馆汽车号牌  \n * ```TRAINING_VEHICLE``` ：教练汽车号牌  \n * ```POLICE_VEHICLE``` ：警用汽车号牌  \n * ```ARMED_POLICE_VEHICLE``` ：武警号牌  \n * ```ARMY_VEHICLE``` ：军队号牌  \n * ```TRAILER``` ：拖车号牌  \n * ```LARGE_NEW_ENERGY_VEHICLE``` ：大型新能源汽车号牌  \n * ```SMALL_NEW_ENERGY_VEHICLE``` ：小型新能源汽车号牌  \n * ```HONG_KONG_OR_MACAO_EXIT_VEHICLE``` ：港澳出境车辆号牌  \n * ```FIRE_RESCUE_VEHICLE``` ：消防救援车号牌  \n * ```HONG_KONG_OR_MACAO_VEHICLE``` ：港澳车辆号牌  \n * ```PLATE_CLASS_TYPE_OTHER``` ：保留字段, 暂未使用.  \n||false|string||
|plateColor|车牌颜色。可能的取值：  \n * ```BLACK``` ：黑  \n * ```BLUE``` ：蓝  \n * ```YELLOW``` ：黄  \n * ```GREEN``` ：绿  \n * ```WHITE``` ：白  \n * ```YELLOW_GREEN``` ：黄绿  \n||false|string||
|plateNo|车牌||false|string||
|resourceTypeId|视频源类型||false|integer(int64)||
|specialVehicleClass|特种车辆属性信息。可能的值：  \n * ```UNKNOWN``` ：未知  \n * ```OTHER``` ：其他  \n * ```COMMON_VEHICLE``` ：普通车辆  \n * ```AMBULANCE``` ：救护车  \n * ```FIRE_FIGHT``` ：消防车  \n * ```PUBLIC_SECURITY``` ：公检法车  \n * ```DUMP``` ：渣土车  \n * ```MIXER``` ：搅拌车  \n * ```TAXI``` ：出租车  \n||false|string||
|vehicleBrand|车辆品牌。||false|string||
|vehicleClass|车辆类型。可能的取值：  \n * ```UNKNOWN``` ：未知  \n * ```OTHER``` ：其他  \n * ```CAR``` ：轿车  \n * ```VAN``` ：厢式货车（注意：实际测试发现别克GL8这类六座商务车也在这个类型里）  \n * ```SMALL_TRUCK``` ：小卡车  \n * ```BIG_TRUCK``` ：大卡车  \n * ```SUV``` ：运动型多功能车  \n * ```BIG_BUS``` ：大型公共汽车  \n * ```MED_BUS``` ：中型公共汽车  \n||false|string||
|vehicleColor|车辆颜色。可能的取值：  \n * ```NONE``` ：未知  \n * ```BLACK``` ：黑  \n * ```WHITE``` ：白  \n * ```GRAY``` ：灰  \n * ```RED``` ：红  \n * ```BLUE``` ：蓝  \n * ```YELLOW``` ：黄  \n * ```ORANGE``` ：橙  \n * ```BROWN``` ：棕  \n * ```GREEN``` ：绿  \n * ```PURPLE``` ：紫  \n * ```CYAN``` ：青  \n * ```PINK``` ：粉  \n||false|string||
|vehicleModel|车辆型号||false|string||
|videoName|视频源名称||false|string||
|videoSerial|视频源Serial||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0,
		"vehicles": [
			{
				"address": "",
				"bigImageUrl": "",
				"captureTime": 0,
				"imageUrl": "",
				"latitude": 0,
				"longitude": 0,
				"objectPosition": {
					"height": 0,
					"vertices": [
						{
							"x": 0,
							"y": 0
						}
					],
					"width": 0
				},
				"plateClass": "",
				"plateColor": "",
				"plateNo": "",
				"resourceTypeId": 0,
				"specialVehicleClass": "",
				"vehicleBrand": "",
				"vehicleClass": "",
				"vehicleColor": "",
				"vehicleModel": "",
				"videoName": "",
				"videoSerial": ""
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# vehicle-struct-controller


## 批量接入结构化解析任务


**接口地址**:`/v1/vehicle-struct/batch-access`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:接入结构化解析任务


**请求示例**:


```javascript
{
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|VehicleStructuralBatchRequest|VehicleStructuralBatchRequest|
|videoSerials|摄像头序号数组||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVehicleStructuralBatchResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VehicleStructuralBatchResponse|VehicleStructuralBatchResponse|
|fail|接入失败的摄像头序号|array|string|
|success|接入成功的摄像头序号|array|string|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"fail": [],
		"success": []
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 批量取消接入车辆结构化解析任务


**接口地址**:`/v1/vehicle-struct/batch-cancel-access`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:取消接入结构化解析任务


**请求示例**:


```javascript
{
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|VehicleStructuralBatchRequest|VehicleStructuralBatchRequest|
|videoSerials|摄像头序号数组||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVehicleStructuralBatchResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VehicleStructuralBatchResponse|VehicleStructuralBatchResponse|
|fail|接入失败的摄像头序号|array|string|
|success|接入成功的摄像头序号|array|string|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"fail": [],
		"success": []
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 获取结构化查询摄像头列表


**接口地址**:`/v1/vehicle-struct/list`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:获取结构化查询摄像头列表


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|pageNum|起始页|query|false|integer(int32)||
|pageSize|分页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfStructuralVideoListResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|StructuralVideoListResponse|StructuralVideoListResponse|
|enabledCount|接入的摄像头数量|integer(int32)||
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|videos|摄像头列表|array|StructuralVideo|
|name|摄像头名称||false|string||
|serial|摄像头序号||false|string||
|structuralEnabled|是否接入结构化解析||false|boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"enabledCount": 0,
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0,
		"videos": [
			{
				"name": "",
				"serial": "",
				"structuralEnabled": true
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# video-controller


## 新增视频源


**接口地址**:`/v1/video`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:新增视频源


**请求示例**:


```javascript
{
	"deviceCode": "",
	"dir": "",
	"groupSerial": "",
	"host": "",
	"latitude": "",
	"longitude": "",
	"name": "",
	"parentSourceId": 0,
	"password": "",
	"path": "",
	"platformSerial": "",
	"port": 0,
	"protocolType": 0,
	"ratioType": "",
	"remark": "",
	"sip": {
		"platformId": 0,
		"platformName": "",
		"replayId": "",
		"sipCallBeginTime": "",
		"sipCallEndTime": "",
		"sipPlayBack": 0
	},
	"sourceId": 0,
	"username": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|VideoResourceOperateParam|VideoResourceOperateParam|
|deviceCode|设备编号 28181必填且接入过后不能修改||false|string||
|dir|接入源根目录||false|string||
|groupSerial|分组序号||true|string||
|host|抓拍机,onvif为ip地址||false|string||
|latitude|纬度||true|string||
|longitude|经度||true|string||
|name|设备名称||true|string||
|parentSourceId|父类型设备ID||false|integer(int32)||
|password|密码（抓拍机）||false|string||
|path|视频地址：为本地视频时是路径，为摄像机时是RTSP路径||false|string||
|platformSerial|平台编号||false|string||
|port|端口（抓拍机，默认值是80）||false|integer(int32)||
|protocolType|网络协议类型：0为TCP，1为UDP型,2为TCP_SERVER_MODE||false|integer(int32)||
|ratioType|分辨率类型 SD：标清 HD：高清,默认标清||false|string||
|remark|备注||false|string||
|sip|GB28181信息||false|VideoResourceSip|VideoResourceSip|
|platformId|系统设置中的平台表主键,gb必填||false|integer(int64)||
|platformName|平台名称||false|string||
|replayId|回放ID||false|string||
|sipCallBeginTime|回放开始时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipCallEndTime|回放结束时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipPlayBack|命令类型 1:实时 2:回放,默认为1||false|integer(int32)||
|sourceId|子类型设备ID||false|integer(int32)||
|username|用户名（抓拍机）||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfAddVideoResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|AddVideoResponse|AddVideoResponse|
|videoSerial|视频源序号|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"videoSerial": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 删除视频源


**接口地址**:`/v1/video`


**请求方式**:`DELETE`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:删除视频源:支持批量删除，多个以逗号隔开


**请求示例**:


```javascript
{
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|DeleteVideoParam|DeleteVideoParam|
|videoSerials|视频源serial列表||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 视频源统计数量预览


**接口地址**:`/v1/video/count/overview`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:视频源统计数量预览


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfTypeCountResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|TypeCountResponse|TypeCountResponse|
|allCount|设备总数|integer(int64)||
|countEntries|各类型统计信息|array|TypeCountEntry|
|parentSourceId|摄像头类型，1直连摄像机 2平台接入摄像机 3直连抓拍机 4平台接入抓拍机||false|integer(int64)||
|resourceCount|对应类型的设备总数||false|integer(int64)||
|offLineCount|离线状态设备统计|integer(int64)||
|onlineCount|在线状态设备统计|integer(int64)||
|unknownCount|未知状态设备统计|integer(int64)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"allCount": 0,
		"countEntries": [
			{
				"parentSourceId": 0,
				"resourceCount": 0
			}
		],
		"offLineCount": 0,
		"onlineCount": 0,
		"unknownCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 视频源移入新的分组


**接口地址**:`/v1/video/group`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:视频源移入新的分组


**请求示例**:


```javascript
{
	"groupSerial": "",
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|MoveVideoGroupParam|MoveVideoGroupParam|
|groupSerial|新分组序号||false|string||
|videoSerials|摄像头序号||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 获取视频源列表


**接口地址**:`/v1/video/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:获取视频源列表


**请求示例**:


```javascript
{
	"containSubGroup": 0,
	"groupSerial": "",
	"pageNum": 1,
	"pageSize": 10,
	"pagination": 0,
	"parentSourceId": 0,
	"runStatus": 0,
	"searchName": "",
	"sortName": "",
	"sortOrder": "",
	"status": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|查询参数|body|false|VideoResourceQueryParam|VideoResourceQueryParam|
|containSubGroup|是否查询当前层级和下级所有的视频源，默认1，0-不是，1-是 默认为1||false|integer(int32)||
|groupSerial|分组serial||false|string||
|pageNum|起始页||false|integer(int32)||
|pageSize|分页大小||false|integer(int32)||
|pagination|是否分页，默认1，0-不分页，1-分页||false|integer(int32)||
|parentSourceId|摄像头类型，1直连摄像机 2平台接入摄像机 3直连抓拍机 4平台接入抓拍机||false|integer(int32)||
|runStatus|视频源运行状态：0为离线, 1为在线, 2为未知||false|integer(int32)||
|searchName|视频源名称 全匹配模糊检索||false|string||
|sortName|排序字段名称:分组：group_id，创建时间：create_time，接入状态：access_status，在线离线状态：run_status ,默认 resource_id||false|string||
|sortOrder|正序倒序，值为asc或者desc， 默认为desc||false|string||
|status|视频源状态 0删除，1未删除 默认1||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfResourceResultInfo|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|ResourceResultInfo|ResourceResultInfo|
|entries|视频源信息|array|VideoResource|
|deviceCode|设备编码||false|string||
|dir|接入源根目录||false|string||
|groupName|分组名称||false|string||
|groupSerial|分组serial||false|string||
|host|设备IP地址||false|string||
|labellingImgMinioKey|标注图片所在的minio的key||false|string||
|labellingImgUrl|标注图片所在的minio的url||false|string||
|labellingStatus|标注状态 0-未标注 1-标注中 2-标注成功 3-标注失败||false|integer(int32)||
|labellingType|标注类型 -1未知(新建) 0-手动标注 1-自动标注||false|integer(int32)||
|latitude|纬度||false|string||
|longitude|经度||false|string||
|name|名称||false|string||
|parentSourceId|摄像头类型，1直连摄像机 2平台接入摄像机 3直连抓拍机 4平台接入抓拍机||false|integer(int32)||
|password|设备密码||false|string||
|path|视频地址：为本地视频时是路径，为摄像机时是RTSP路径,为摄像头的时候必填||false|string||
|platformName|平台服务名称||false|string||
|platformSerial|平台服务Serial||false|string||
|port|端口||false|integer(int32)||
|protocolType|网络协议类型：0为TCP，1为UDP型,2为TCP_SERVER_MODE||false|integer(int32)||
|ratioType|分辨率类型 SD：标清 HD：高清  默认值为SD一般职位阿里云平台有效||false|string||
|remark|备注||false|string||
|resourceTypeId|视频源类型ID||false|integer(int64)||
|roi|交通标记区域json串||false|Roi|Roi|
|congestionRoi|拥堵观察区||false|array|CongestionArea|
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|threshold|拥堵告警阈值||false|integer(int64)||
|display|分辨率||false|Display|Display|
|height|高度||false|integer(int32)||
|width|宽度||false|integer(int32)||
|roi|道路划分||false|array|Area|
|arrowLines|箭头坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|runStatus|运行状态：0为离线，1为在线||false|integer(int32)||
|sip|GB28181,1400 平台配置信息||false|VideoResourceSip|VideoResourceSip|
|platformId|系统设置中的平台表主键,gb必填||false|integer(int64)||
|platformName|平台名称||false|string||
|replayId|回放ID||false|string||
|sipCallBeginTime|回放开始时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipCallEndTime|回放结束时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipPlayBack|命令类型 1:实时 2:回放,默认为1||false|integer(int32)||
|sourceId|子类型设备ID||false|integer(int32)||
|status|状态 0--删除 1--有效||false|integer(int32)||
|tasks|关联的任务列表<taskId,taskName>||false|array|TaskVo|
|status|任务状态，当前任务是否有效||false|integer(int32)||
|taskName|名称||false|string||
|taskSerial|序号||false|string||
|working|任务是否正在运行||false|boolean||
|username|设备用户名||false|string||
|videoSerial|视频源serial||false|string||
|offlineCount|离线状态设备总数|integer(int64)||
|onlineCount|在线状态设备总数|integer(int64)||
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|total|记录总数|integer(int32)||
|unknownCount|未知状态设备总数|integer(int64)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"entries": [
			{
				"deviceCode": "",
				"dir": "",
				"groupName": "",
				"groupSerial": "",
				"host": "",
				"labellingImgMinioKey": "",
				"labellingImgUrl": "",
				"labellingStatus": 0,
				"labellingType": 0,
				"latitude": "",
				"longitude": "",
				"name": "",
				"parentSourceId": 0,
				"password": "",
				"path": "",
				"platformName": "",
				"platformSerial": "",
				"port": 0,
				"protocolType": 0,
				"ratioType": "",
				"remark": "",
				"resourceTypeId": 0,
				"roi": {
					"congestionRoi": [
						{
							"polygons": [
								{
									"x": 0,
									"y": 0
								}
							],
							"threshold": 0
						}
					],
					"display": {
						"height": 0,
						"width": 0
					},
					"roi": [
						{
							"arrowLines": [
								{
									"x": 0,
									"y": 0
								}
							],
							"polygons": [
								{
									"x": 0,
									"y": 0
								}
							]
						}
					]
				},
				"runStatus": 0,
				"sip": {
					"platformId": 0,
					"platformName": "",
					"replayId": "",
					"sipCallBeginTime": "",
					"sipCallEndTime": "",
					"sipPlayBack": 0
				},
				"sourceId": 0,
				"status": 0,
				"tasks": [
					{
						"status": 0,
						"taskName": "",
						"taskSerial": "",
						"working": true
					}
				],
				"username": "",
				"videoSerial": ""
			}
		],
		"offlineCount": 0,
		"onlineCount": 0,
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"total": 0,
		"unknownCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 恢复删除的视频源


**接口地址**:`/v1/video/recovery`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:恢复删除的视频源:支持批量恢复


**请求示例**:


```javascript
{
	"videoSerials": []
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|视频源id、名称列表,支持批量恢复|body|false|VideoResourceRecoveryParam|VideoResourceRecoveryParam|
|videoSerials|视频源serial列表||false|array|string|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfRecoveryResultResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|RecoveryResultResponse|RecoveryResultResponse|
|errorResultList|视频源恢复失败详情描述|array|VideoResourceHandleError|
|errorMsg|||false|string||
|name|||false|string||
|failCount|视频源恢复失败个数|integer(int32)||
|msg|视频源恢复结果描述|string||
|successCount|视频源恢复成功个数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"errorResultList": [
			{
				"errorMsg": "",
				"name": ""
			}
		],
		"failCount": 0,
		"msg": "",
		"successCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 视频源标记热区


**接口地址**:`/v1/video/roi`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:视频源标记热区


**请求示例**:


```javascript
{
	"roi": {
		"congestionRoi": [
			{
				"polygons": [
					{
						"x": 0,
						"y": 0
					}
				],
				"threshold": 0
			}
		],
		"display": {
			"height": 0,
			"width": 0
		},
		"roi": [
			{
				"arrowLines": [
					{
						"x": 0,
						"y": 0
					}
				],
				"polygons": [
					{
						"x": 0,
						"y": 0
					}
				]
			}
		]
	},
	"videoSerial": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|DrawRoiParam|DrawRoiParam|
|roi|智慧交通标记热区json串||false|Roi|Roi|
|congestionRoi|拥堵观察区||false|array|CongestionArea|
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|threshold|拥堵告警阈值||false|integer(int64)||
|display|分辨率||false|Display|Display|
|height|高度||false|integer(int32)||
|width|宽度||false|integer(int32)||
|roi|道路划分||false|array|Area|
|arrowLines|箭头坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|videoSerial|视频源serial||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 视频回放


**接口地址**:`/v1/video/rtsp-playback/{videoSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:视频回放


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|endTime|结束时间，long，毫秒|query|false|integer(int64)||
|startTime|开始时间，long，毫秒|query|false|integer(int64)||
|videoSerial|视频源serial|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfRtspVo|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|RtspVo|RtspVo|
|rtspAddress|返回的RTSP视频流地址|string||
|videoType|视频类型。1：本地视频；2：RTSP摄像头（不一定具有回放功能）；3：抓拍机；6：GB28181|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"rtspAddress": "",
		"videoType": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## rtsp摄像机获取视频流地址


**接口地址**:`/v1/video/rtsp/{videoSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:rtsp摄像机获取视频流地址


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|videoSerial|视频源serial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfRtspUrlResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|RtspUrlResponse|RtspUrlResponse|
|height|图像高度|integer(int32)||
|path|rtsp视频源地址|string||
|streamType|码流类型|string||
|videoFormat|视频格式|string||
|width|图像宽度|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"height": 0,
		"path": "",
		"streamType": "",
		"videoFormat": "",
		"width": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 获取视频源分类信息


**接口地址**:`/v1/video/source-type`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:获取视频源分类信息


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVideoSourceTypeWrapper|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VideoSourceTypeWrapper|VideoSourceTypeWrapper|
|sourceTypes|设备类型分类信息|array|VideoSourceType|
|parentSourceId|父类设备编号||false|integer(int32)||
|parentSourceName|父类设备类型名称||false|string||
|subSources|子类型列表||false|array|SubSource|
|subSourceId|子类设备编号||false|integer(int32)||
|subSourceName|子类设备名称||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"sourceTypes": [
			{
				"parentSourceId": 0,
				"parentSourceName": "",
				"subSources": [
					{
						"subSourceId": 0,
						"subSourceName": ""
					}
				]
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 视频源详情


**接口地址**:`/v1/video/{videoSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:视频源详情


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|videoSerial|videoSerial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVideoResource|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VideoResource|VideoResource|
|deviceCode|设备编码|string||
|dir|接入源根目录|string||
|groupName|分组名称|string||
|groupSerial|分组serial|string||
|host|设备IP地址|string||
|labellingImgMinioKey|标注图片所在的minio的key|string||
|labellingImgUrl|标注图片所在的minio的url|string||
|labellingStatus|标注状态 0-未标注 1-标注中 2-标注成功 3-标注失败|integer(int32)||
|labellingType|标注类型 -1未知(新建) 0-手动标注 1-自动标注|integer(int32)||
|latitude|纬度|string||
|longitude|经度|string||
|name|名称|string||
|parentSourceId|摄像头类型，1直连摄像机 2平台接入摄像机 3直连抓拍机 4平台接入抓拍机|integer(int32)||
|password|设备密码|string||
|path|视频地址：为本地视频时是路径，为摄像机时是RTSP路径,为摄像头的时候必填|string||
|platformName|平台服务名称|string||
|platformSerial|平台服务Serial|string||
|port|端口|integer(int32)||
|protocolType|网络协议类型：0为TCP，1为UDP型,2为TCP_SERVER_MODE|integer(int32)||
|ratioType|分辨率类型 SD：标清 HD：高清  默认值为SD一般职位阿里云平台有效|string||
|remark|备注|string||
|resourceTypeId|视频源类型ID|integer(int64)||
|roi|交通标记区域json串|Roi|Roi|
|congestionRoi|拥堵观察区||false|array|CongestionArea|
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|threshold|拥堵告警阈值||false|integer(int64)||
|display|分辨率||false|Display|Display|
|height|高度||false|integer(int32)||
|width|宽度||false|integer(int32)||
|roi|道路划分||false|array|Area|
|arrowLines|箭头坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|runStatus|运行状态：0为离线，1为在线|integer(int32)||
|sip|GB28181,1400 平台配置信息|VideoResourceSip|VideoResourceSip|
|platformId|系统设置中的平台表主键,gb必填||false|integer(int64)||
|platformName|平台名称||false|string||
|replayId|回放ID||false|string||
|sipCallBeginTime|回放开始时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipCallEndTime|回放结束时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipPlayBack|命令类型 1:实时 2:回放,默认为1||false|integer(int32)||
|sourceId|子类型设备ID|integer(int32)||
|status|状态 0--删除 1--有效|integer(int32)||
|tasks|关联的任务列表<taskId,taskName>|array|TaskVo|
|status|任务状态，当前任务是否有效||false|integer(int32)||
|taskName|名称||false|string||
|taskSerial|序号||false|string||
|working|任务是否正在运行||false|boolean||
|username|设备用户名|string||
|videoSerial|视频源serial|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"deviceCode": "",
		"dir": "",
		"groupName": "",
		"groupSerial": "",
		"host": "",
		"labellingImgMinioKey": "",
		"labellingImgUrl": "",
		"labellingStatus": 0,
		"labellingType": 0,
		"latitude": "",
		"longitude": "",
		"name": "",
		"parentSourceId": 0,
		"password": "",
		"path": "",
		"platformName": "",
		"platformSerial": "",
		"port": 0,
		"protocolType": 0,
		"ratioType": "",
		"remark": "",
		"resourceTypeId": 0,
		"roi": {
			"congestionRoi": [
				{
					"polygons": [
						{
							"x": 0,
							"y": 0
						}
					],
					"threshold": 0
				}
			],
			"display": {
				"height": 0,
				"width": 0
			},
			"roi": [
				{
					"arrowLines": [
						{
							"x": 0,
							"y": 0
						}
					],
					"polygons": [
						{
							"x": 0,
							"y": 0
						}
					]
				}
			]
		},
		"runStatus": 0,
		"sip": {
			"platformId": 0,
			"platformName": "",
			"replayId": "",
			"sipCallBeginTime": "",
			"sipCallEndTime": "",
			"sipPlayBack": 0
		},
		"sourceId": 0,
		"status": 0,
		"tasks": [
			{
				"status": 0,
				"taskName": "",
				"taskSerial": "",
				"working": true
			}
		],
		"username": "",
		"videoSerial": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 编辑视频源


**接口地址**:`/v1/video/{videoSerial}`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:编辑视频源


**请求示例**:


```javascript
{
	"deviceCode": "",
	"dir": "",
	"groupSerial": "",
	"host": "",
	"latitude": "",
	"longitude": "",
	"name": "",
	"parentSourceId": 0,
	"password": "",
	"path": "",
	"platformSerial": "",
	"port": 0,
	"protocolType": 0,
	"ratioType": "",
	"remark": "",
	"sip": {
		"platformId": 0,
		"platformName": "",
		"replayId": "",
		"sipCallBeginTime": "",
		"sipCallEndTime": "",
		"sipPlayBack": 0
	},
	"sourceId": 0,
	"username": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|VideoResourceOperateParam|VideoResourceOperateParam|
|deviceCode|设备编号 28181必填且接入过后不能修改||false|string||
|dir|接入源根目录||false|string||
|groupSerial|分组序号||true|string||
|host|抓拍机,onvif为ip地址||false|string||
|latitude|纬度||true|string||
|longitude|经度||true|string||
|name|设备名称||true|string||
|parentSourceId|父类型设备ID||false|integer(int32)||
|password|密码（抓拍机）||false|string||
|path|视频地址：为本地视频时是路径，为摄像机时是RTSP路径||false|string||
|platformSerial|平台编号||false|string||
|port|端口（抓拍机，默认值是80）||false|integer(int32)||
|protocolType|网络协议类型：0为TCP，1为UDP型,2为TCP_SERVER_MODE||false|integer(int32)||
|ratioType|分辨率类型 SD：标清 HD：高清,默认标清||false|string||
|remark|备注||false|string||
|sip|GB28181信息||false|VideoResourceSip|VideoResourceSip|
|platformId|系统设置中的平台表主键,gb必填||false|integer(int64)||
|platformName|平台名称||false|string||
|replayId|回放ID||false|string||
|sipCallBeginTime|回放开始时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipCallEndTime|回放结束时间，格式为yyyy-MM-dd HH:mm:ss||false|string(date-time)||
|sipPlayBack|命令类型 1:实时 2:回放,默认为1||false|integer(int32)||
|sourceId|子类型设备ID||false|integer(int32)||
|username|用户名（抓拍机）||false|string||
|videoSerial|videoSerial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 自动标记视频源


**接口地址**:`/v1/video/{videoSerial}/labelling-automated`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:自动标记视频源


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|videoSerial|videoSerial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfint|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|integer(int32)|integer(int32)|
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": 0,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# video-group-controller


## 新增视频源分组


**接口地址**:`/v1/video-group`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:新增视频源分组


**请求示例**:


```javascript
{
	"name": "",
	"parentSerial": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|VideoResourceGroupAddParam|VideoResourceGroupAddParam|
|name|名称||true|string||
|parentSerial|父级分组Serial,新增不传递的情况默认是一级分组下||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfAddGroupResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|AddGroupResponse|AddGroupResponse|
|groupSerial|分组编号|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"groupSerial": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 获取视频源树结构


**接口地址**:`/v1/video-group/tree`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:获取视频源树结构（可以通过配置是否获取包含分组下视频源个数，视频源等信息）


**请求示例**:


```javascript
{
	"exclusiveCapture": true,
	"exclusiveDelGroup": true,
	"exclusiveNoGroup": true,
	"name": "",
	"parentSerial": "",
	"resourceCountSetting": true,
	"resourcesSetting": true,
	"structOnly": true,
	"subTreeNodesSetting": true
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|ResourceTreeSearchParam|ResourceTreeSearchParam|
|exclusiveCapture|是否排除抓拍机(默认false 排除)||false|boolean||
|exclusiveDelGroup|是否排除已删除视频源分组树节点 默认true 排除||false|boolean||
|exclusiveNoGroup|是否排除未分组树节点(默认true 排除)||false|boolean||
|name|搜索视频源名称||false|string||
|parentSerial|父节点 默认 null ,从顶级节点开始,用于逐级加载树节点||false|string||
|resourceCountSetting|是否设置组的分组以及子分组视频源数(默认true 设置)||false|boolean||
|resourcesSetting|是否设置节点所属视频源 默认true 设置||false|boolean||
|structOnly|是否只返回已接入结构化的视频源||false|boolean||
|subTreeNodesSetting|是否加载下级节点(默认true 设置)||false|boolean||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfResourceGroupResultInfo|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|ResourceGroupResultInfo|ResourceGroupResultInfo|
|responseData|视频源分组列表|array|VideoResourceTreeNode|
|groupSerial|视频源组serial||false|string||
|name|名称||false|string||
|resources|本层级视频源列表||false|array|VideoResourceNode|
|deviceCode|设备编码||false|string||
|groupName|分组名称||false|string||
|groupSerial|视频源分组serial||false|string||
|labellingImgMinioKey|标注图片所在的minio的key||false|string||
|labellingImgUrl|标注图片所在的minio的url||false|string||
|labellingStatus|标注状态 0-未标注 1-标注中 2-标注成功 3-标注失败||false|integer(int32)||
|labellingType|标注类型 -1未知(新建) 0-手动标注 1-自动标注||false|integer(int32)||
|latitude|维度||false|string||
|longitude|经度||false|string||
|name|名称||false|string||
|path|视频地址：为本地视频时是路径，为摄像机时是RTSP路径||false|string||
|resourceSerial|视频源serial||false|string||
|resourceTypeId|视频源类型id||false|integer(int64)||
|roi|智慧交通标记区域json串||false|Roi|Roi|
|congestionRoi|拥堵观察区||false|array|CongestionArea|
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|threshold|拥堵告警阈值||false|integer(int64)||
|display|分辨率||false|Display|Display|
|height|高度||false|integer(int32)||
|width|宽度||false|integer(int32)||
|roi|道路划分||false|array|Area|
|arrowLines|箭头坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|polygons|多边形区域坐标百分比，使用时需要将对应百分比和对应边相乘，得到实际的位置坐标||false|array|Vertex|
|x|x轴坐标||false|number(double)||
|y|y轴坐标||false|number(double)||
|runStatus|0离线，1在线||false|integer(int32)||
|structuralEnabled|是否接入结构化解析||false|boolean||
|structuralSearch|是否添加到结构化解析列表||false|boolean||
|resourcesCount|视频源个数||false|integer(int32)||
|subTreeNodes|下级分组列表||false|array|VideoResourceTreeNode|
|totalCount|视频源个数|integer(int64)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"responseData": [
			{
				"groupSerial": "",
				"name": "",
				"resources": [
					{
						"deviceCode": "",
						"groupName": "",
						"groupSerial": "",
						"labellingImgMinioKey": "",
						"labellingImgUrl": "",
						"labellingStatus": 0,
						"labellingType": 0,
						"latitude": "",
						"longitude": "",
						"name": "",
						"path": "",
						"resourceSerial": "",
						"resourceTypeId": 0,
						"roi": {
							"congestionRoi": [
								{
									"polygons": [
										{
											"x": 0,
											"y": 0
										}
									],
									"threshold": 0
								}
							],
							"display": {
								"height": 0,
								"width": 0
							},
							"roi": [
								{
									"arrowLines": [
										{
											"x": 0,
											"y": 0
										}
									],
									"polygons": [
										{
											"x": 0,
											"y": 0
										}
									]
								}
							]
						},
						"runStatus": 0,
						"structuralEnabled": true,
						"structuralSearch": true
					}
				],
				"resourcesCount": 0,
				"subTreeNodes": [
					{}
				]
			}
		],
		"totalCount": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 查看视频源分组详情


**接口地址**:`/v1/video-group/{groupSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:查看视频源分组详情


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|groupSerial|groupSerial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfVideoResourceGroup|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|VideoResourceGroup|VideoResourceGroup|
|groupSerial|视频源组serial|string||
|name|名称|string||
|parentSerial|父级分组serial|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"groupSerial": "",
		"name": "",
		"parentSerial": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 编辑视频源分组


**接口地址**:`/v1/video-group/{groupSerial}`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:编辑视频源分组


**请求示例**:


```javascript
{
	"name": "",
	"parentSerial": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|VideoResourceGroupAddParam|VideoResourceGroupAddParam|
|name|名称||true|string||
|parentSerial|父级分组Serial,新增不传递的情况默认是一级分组下||true|string||
|groupSerial|视频源组serial|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfboolean|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": true,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 删除视频源分组


**接口地址**:`/v1/video-group/{groupSerial}`


**请求方式**:`DELETE`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:删除视频源分组


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|groupSerial|groupSerial|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfboolean|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": true,
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


# video-platform-controller


## 新增平台


**接口地址**:`/v1/video-platform`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*,application/json`


**接口描述**:新增平台


**请求示例**:


```javascript
{
	"gat1400": {
		"ourPlatformId": "",
		"password": "",
		"platformId": "",
		"platformIp": "",
		"platformName": "",
		"platformSerial": "",
		"port": 0,
		"remark": "",
		"username": ""
	},
	"gb28181": {
		"password": "",
		"platformId": "",
		"platformIp": "",
		"platformName": "",
		"platformSerial": "",
		"port": 0,
		"protocol": "",
		"username": ""
	},
	"platformType": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|PlatformVo|PlatformVo|
|gat1400|Gat1400平台参数||false|Gat1400Platform|Gat1400Platform|
|ourPlatformId|我方平台ID||true|string||
|password|密码||true|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|remark|备注||false|string||
|username|用户名||true|string||
|gb28181|国标平台参数||false|Gb28181Platform|Gb28181Platform|
|password|平台密码||false|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|protocol|传输协议,可用值:TCP,UDP,TCP_SERVER||true|string||
|username|平台用户名||false|string||
|platformType|平台类型,可用值:GB28181,GAT1400||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfstring|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": "",
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 订阅(1400平台)


**接口地址**:`/v1/video-platform/gat1400/subscribe`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:订阅(1400平台)


**请求示例**:


```javascript
{
	"receiveAddr": "",
	"resourceUri": "",
	"subscribeId": "",
	"title": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|platformSerial|平台序号|query|true|string||
|subscribeVo|subscribeVo|body|true|Gat1400SubscribeVo|Gat1400SubscribeVo|
|receiveAddr|信息接收地址||true|string||
|resourceUri|可选, 订阅资源路径.||false|string||
|subscribeId|订阅ID, 响应报文返回||false|string||
|title|可选, 订阅标题.||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfstring|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": "",
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 订阅列表(1400平台)


**接口地址**:`/v1/video-platform/gat1400/subscribe/{platformSerial}/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:订阅列表(1400平台)


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|platformSerial|平台序号|path|true|string||
|pageNum|第几页|query|false|integer(int32)||
|pageSize|分页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfSubscribeVoList|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|SubscribeVoList|SubscribeVoList|
|pageNum|第几页|integer(int32)||
|pageSize|每页记录数|integer(int32)||
|pages|总页数|integer(int32)||
|size|当前页记录数|integer(int32)||
|subscribeVos|订阅列表|array|Gat1400SubscribeVo|
|receiveAddr|信息接收地址||true|string||
|resourceUri|可选, 订阅资源路径.||false|string||
|subscribeId|订阅ID, 响应报文返回||false|string||
|title|可选, 订阅标题.||false|string||
|total|记录总数|integer(int32)||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"pageNum": 0,
		"pageSize": 0,
		"pages": 0,
		"size": 0,
		"subscribeVos": [
			{
				"receiveAddr": "",
				"resourceUri": "",
				"subscribeId": "",
				"title": ""
			}
		],
		"total": 0
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 订阅详情(1400平台)


**接口地址**:`/v1/video-platform/gat1400/subscribe/{subscribeId}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:订阅详情(1400平台)


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|subscribeId|订阅记录id|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfGat1400SubscribeVo|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|Gat1400SubscribeVo|Gat1400SubscribeVo|
|receiveAddr|信息接收地址|string||
|resourceUri|可选, 订阅资源路径.|string||
|subscribeId|订阅ID, 响应报文返回|string||
|title|可选, 订阅标题.|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"receiveAddr": "",
		"resourceUri": "",
		"subscribeId": "",
		"title": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 删除订阅(1400平台)


**接口地址**:`/v1/video-platform/gat1400/subscribe/{subscribeId}`


**请求方式**:`DELETE`


**请求数据类型**:`*`


**响应数据类型**:`*/*`


**接口描述**:删除订阅(1400平台)


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|subscribeId|订阅记录id|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 平台列表


**接口地址**:`/v1/video-platform/list`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:平台列表


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfPlatformListResponse|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|PlatformListResponse|PlatformListResponse|
|gat1400Platforms|1400平台列表信息|array|Gat1400Platform|
|ourPlatformId|我方平台ID||true|string||
|password|密码||true|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|remark|备注||false|string||
|username|用户名||true|string||
|gb28181Platforms|28181平台列表信息|array|Gb28181Platform|
|password|平台密码||false|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|protocol|传输协议,可用值:TCP,UDP,TCP_SERVER||true|string||
|username|平台用户名||false|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"gat1400Platforms": [
			{
				"ourPlatformId": "",
				"password": "",
				"platformId": "",
				"platformIp": "",
				"platformName": "",
				"platformSerial": "",
				"port": 0,
				"remark": "",
				"username": ""
			}
		],
		"gb28181Platforms": [
			{
				"password": "",
				"platformId": "",
				"platformIp": "",
				"platformName": "",
				"platformSerial": "",
				"port": 0,
				"protocol": "",
				"username": ""
			}
		]
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 平台详情


**接口地址**:`/v1/video-platform/{platformSerial}`


**请求方式**:`GET`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:平台详情


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|platformSerial|平台编号|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfPlatformVo|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|PlatformVo|PlatformVo|
|gat1400|Gat1400平台参数|Gat1400Platform|Gat1400Platform|
|ourPlatformId|我方平台ID||true|string||
|password|密码||true|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|remark|备注||false|string||
|username|用户名||true|string||
|gb28181|国标平台参数|Gb28181Platform|Gb28181Platform|
|password|平台密码||false|string||
|platformId|平台ID||true|string||
|platformIp|平台IP||true|string||
|platformName|平台名称||true|string||
|platformSerial|平台序号, 响应报文填写||false|string||
|port|端口||true|integer(int32)||
|protocol|传输协议,可用值:TCP,UDP,TCP_SERVER||true|string||
|username|平台用户名||false|string||
|platformType|平台类型,可用值:GB28181,GAT1400|string||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"gat1400": {
			"ourPlatformId": "",
			"password": "",
			"platformId": "",
			"platformIp": "",
			"platformName": "",
			"platformSerial": "",
			"port": 0,
			"remark": "",
			"username": ""
		},
		"gb28181": {
			"password": "",
			"platformId": "",
			"platformIp": "",
			"platformName": "",
			"platformSerial": "",
			"port": 0,
			"protocol": "",
			"username": ""
		},
		"platformType": ""
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```


## 删除平台


**接口地址**:`/v1/video-platform/{platformSerial}`


**请求方式**:`DELETE`


**请求数据类型**:`*`


**响应数据类型**:`*/*,application/json`


**接口描述**:删除平台


**请求参数**:


| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|platformSerial|平台编号|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|BaseResultOfOptionalOfobject|


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|data|业务数据|OptionalOfobject|OptionalOfobject|
|present||boolean||
|errorCode|异常码|string||
|errorMsg|返回消息|string||
|success||boolean||


**响应示例**:
```javascript
{
	"data": {
		"present": true
	},
	"errorCode": "",
	"errorMsg": "",
	"success": true
}
```