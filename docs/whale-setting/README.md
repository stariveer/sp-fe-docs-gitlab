

# 表设计

+ info_platform
+ info_subscribe

# 接口设计

## video-platform-controller

+ 新增平台
+ 订阅(1400平台)
+ 订阅列表(1400平台)
+ 订阅详情(1400平台)
+ 删除订阅(1400平台)
+ 平台列表
+ 平台详情
+ 删除平台
