module.exports = {
  title: "SensePolaris",
  description: "商汤睿途智慧交通平台",
  base: "/sp-fe-docs-gitlab/master/",
  dest: "./public/master",
  themeConfig: {
    sidebar: "auto",
    nav: [
      { text: "简介", link: "/" },
      { text: "数据库设计", link: "/database-design/" },
      { text: "接口设计", link: "/interface-design/" },
      {
        text: "研发文档",
        items: [
          { text: "polaris-alarm", link: "/polaris-alarm/" },
          { text: "polaris-dashboard", link: "/polaris-dashboard/" },
          { text: "polaris-push", link: "/polaris-push/" },
          {
            text: "polaris-realtime-detect",
            link: "/polaris-realtime-detect/",
          },
          { text: "polaris-scheduled", link: "/polaris-scheduled/" },
          { text: "polaris-task", link: "/polaris-task/" },
          { text: "unified-authentication", link: "/unified-authentication/" },
          { text: "whale-search", link: "/whale-search/" },
          { text: "whale-setting", link: "/whale-setting/" },
          { text: "whale-sync", link: "/whale-sync/" },
          { text: "whale-video", link: "/whale-video/" },
        ],
      },
      {
        text: "前端文档",
        items: [
          { text: "技术栈", link: "/polaris-fe/tech-stack/" },
          { text: "组件", link: "/polaris-fe/components/" },
          { text: "部署", link: "/polaris-fe/deploy/" },
          { text: "架构", link: "/polaris-fe/framework/" },
          { text: "开发手册", link: "/polaris-fe/develop-cookbook/" },
          { text: "markdown", link: "/polaris-fe/markdown/" },
        ],
      },
      { text: "运维文档", link: "/devops/" },
    ],
    sidebarDepth: 1, // e'b将同时提取markdown中h2 和 h3 标题，显示在侧边栏上。
    lastUpdated: "Last Updated", // 文档更新时间：每个文件git最后提交的时间,,
    // sidebar: {
    //   "/frontend/": [
    //     "/frontend/tech-stack/",
    //     "/frontend/framework/",
    //     "/frontend/components/",
    //     "/frontend/deploy/",
    //     "/frontend/develop-cookbook/",
    //   ],
    //   // fallback
    //   "/": [
    //     "" /* / */,
    //     // "contact" /* /contact.html */,
    //     // "about" /* /about.html */,
    //   ],
    // },
  },
};
