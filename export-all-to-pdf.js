const exportOnePdf = require("./export-one-pdf");
const fs = require("fs");
const path = require("path");

let filePath = path.resolve(process.cwd(), "docs");


//调用文件遍历方法
fileDisplay(filePath);

/**
 * 文件遍历方法
 * @param filePath 需要遍历的文件路径
 */
function fileDisplay(filePath) {
  //根据文件路径读取文件，返回文件列表
  fs.readdir(filePath, function (err, files) {
    if (err) {
      console.warn(err);
    } else {
      //遍历读取到的文件列表
      // exportOnePdf('/Users/vincent/code/sp-fe-docs-gitlab/docs/database-design/README.md');
      // return
      files.forEach(function (filename) {
        //获取当前文件的绝对路径
        var filedir = path.join(filePath, filename);

        //根据文件路径获取文件信息，返回一个fs.Stats对象

        fs.stat(filedir, function (eror, stats) {
          if (eror) {
            exportOnePdf(filedir);
            console.warn("获取文件stats失败");
          } else {
            var isFile = stats.isFile(); //是文件
            var ext = extname = path.extname(filedir);
            var isMarkdown = !!(ext === '.md' || ext === '.markdown')
            var isDir = stats.isDirectory(); //是文件夹
            if (isFile && isMarkdown) {
              // console.log(filedir);
              exportOnePdf(filedir)
            }
            if (isDir) {
              fileDisplay(filedir); //递归，如果是文件夹，就继续遍历该文件夹下面的文件
            }
          }
        });
      });
    }
  });
}
